from test_data.src.pages.dashboard.dashboard_page import DashboardPage
from test_data.src.pages.f_recognition.fc_recognition import FaceRecognition_Page
from test_data.test_cases.test_data import const
from test_data.test_cases.utils.assert_element import AssertionElement
from skeleton.test_template import TestTemplate
from selenium.webdriver.common.by import By
from test_data.test_cases.login_test.login_test import Login
from test_data.test_cases.dashboard_test.dashboard_test import Dashboard
import time
from selenium.webdriver.common.action_chains import ActionChains
from test_data.src.pages.enrollment.enrollment_page import EnrollmentPage

class FaceRecognition(TestTemplate):

    def __init__(self, driver):
        super().__init__(const.DOMAIN, driver)

#  Add Face Recognition analytic [upload via Dashboard]
    @TestTemplate.test_func()
    def test_C4128(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
            fr_page = FaceRecognition_Page(self._driver)
            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            dashboard_page.input_file(const.PATH_FRECOG_IMG)
            time.sleep(1)

            text = fr_page.fr_checkbox_text()
            assert text == "Face Recognition", "FR checkbox not display" 

            fr_page.click_fr_checkbox()
            dashboard_page.click_done_btn()
            time.sleep(2)
            # not yet check success message of notification

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

#   Open Face Recognition List
    @TestTemplate.test_func()
    def test_C1662(self):
        try:
            Login(self._driver).test_C1606()
            FaceRecognition(self._driver).test_C4128()
            fr_page = FaceRecognition_Page(self._driver)

            fr_page.check_result()
            time.sleep(2)
            text = fr_page.check_fr_tab()
            assert text == "Face Recognition", "FR Tab not display"

            #not yet check photo uploaded
            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

#   Verify Face Recognition Tab working well
    @TestTemplate.test_func()
    def test_C1666(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
            fr_page = FaceRecognition_Page(self._driver)
            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            dashboard_page.input_file(const.PATH_FRECOG_IMG)
            time.sleep(1)

            text1 = fr_page.fr_checkbox_text()
            assert text1 == "Face Recognition", "FR checkbox not display" 
            fr_page.click_fr_checkbox()

            text2 = dashboard_page.face_demo_checkbox()
            assert text2 == "Face Demography", "FD checkbox not display" 

            dashboard_page.click_done_btn()
            time.sleep(1)

            self._driver.refresh()
            time.sleep(3)
         
            fr_page.check_result()
            time.sleep(2)
            
            text3 = fr_page.click_fr_tab()
            assert text3 == "Face Recognition", "FR Tab not display"
            #urutan posisi tab analitik berubah-ubah jd tdk tetap dan bisa nyebab'kan error

            text4 = fr_page.section1()
            assert text4 == "Face Recognition", "FR section not display" 

            text5 = fr_page.section2()
            assert text5 == "Identified Person - Detail", "Identified Person section not display" 

            text6 = fr_page.section3()
            assert text6 == "Unknown Person", "Unknown Person section not display" 

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

# Verify Face Recognition Icon
    @TestTemplate.test_func()
    def test_C1665(self):
        try:
            Login(self._driver).test_C1606()
            FaceRecognition(self._driver).test_C4128()
            fr_page = FaceRecognition_Page(self._driver)

            fr_page.check_result()
            time.sleep(2)
            text = fr_page.check_fr_tab()
            assert text == "Face Recognition", "FR Tab not display"
            fr_page.fr_icon_tab()
            #belum ngecek fr_icon

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

#   Verify Total Recognized Face
    @TestTemplate.test_func()
    def test_C1722(self):
        try:
            Login(self._driver).test_C1606()
            FaceRecognition(self._driver).test_C4128()
            fr_page = FaceRecognition_Page(self._driver)

            fr_page.check_result()
            time.sleep(2)
            #not check the colour of bounding box
            #not check total of recognized face, because must matching with face in image
            
            fr_page.recognized_detail()
    
            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

# Verify Total Unrecognized Face
    @TestTemplate.test_func()
    def test_C1797(self):
        try:
            Login(self._driver).test_C1606()
            FaceRecognition(self._driver).test_C4128()
            fr_page = FaceRecognition_Page(self._driver)

            fr_page.check_result()
            time.sleep(3)
            #not check the colour of bounding box
            #not check total of unrecognized face
            
            fr_page.unrecognized_detail()
    
            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

# Close Face Recognition List
    @TestTemplate.test_func()
    def test_C1663(self):
        try:
            Login(self._driver).test_C1606()
            FaceRecognition(self._driver).test_C4128()
            fr_page = FaceRecognition_Page(self._driver)

            fr_page.check_result()
            time.sleep(2)
            fr_page.click_close_list()
            time.sleep(2)
            #can't click close button until fixing bug
            
            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

# Upload more than 1 photo
    @TestTemplate.test_func()
    def test_C4067(self):
        try:
            Dashboard(self._driver).test_C6875()
            fr_page = FaceRecognition_Page(self._driver)
            dashboard_page = DashboardPage(self._driver)

            text = fr_page.fr_checkbox_text()
            assert text == "Face Recognition", "FR checkbox not display" 

            fr_page.click_fr_checkbox()
            dashboard_page.click_done_btn()
            
            self._driver.refresh()
            time.sleep(3)
            #not check the colour of bounding box
            #not yet check Total list of row same with Total Photo uploaded 
            fr_page.more_than_one_result()
            time.sleep(3)
            
            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

# Upload not face image [upload via Dashboard]
    @TestTemplate.test_func()
    def test_C6822(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
            fr_page = FaceRecognition_Page(self._driver)
            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            dashboard_page.input_file(const.PATH_NO_FACE)
            time.sleep(1)

            text = fr_page.fr_checkbox_text()
            assert text == "Face Recognition", "FR checkbox not display" 

            fr_page.click_fr_checkbox()
            dashboard_page.click_done_btn()
            time.sleep(2)

            fr_page.check_result()
            time.sleep(5)

            title1, number, title2, number_identified = fr_page.result_no_face()
            print('text 1:', title1)
            assert title1 == "Total Person", "Total Person text not display"

            print('text 2:', number)
            assert number == "0", "Detect Non Face as Person"

            print('text 3:', title2)
            assert title2 == "Identified Person", "Identified Person text not display" 

            print('text 4:', number_identified)
            assert number_identified == "0", "Detect Unrecognize as Recognized"

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

# Upload not human face [upload via Dashboard]
    @TestTemplate.test_func()
    def test_C7964(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
            fr_page = FaceRecognition_Page(self._driver)
            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            dashboard_page.input_file(const.PATH_NO_HUMAN_FACE)
            time.sleep(1)

            text = fr_page.fr_checkbox_text()
            assert text == "Face Recognition", "FR checkbox not display" 

            fr_page.click_fr_checkbox()
            dashboard_page.click_done_btn()
            time.sleep(2)

            fr_page.check_result()
            time.sleep(5)

            title1, number, title2, number_identified = fr_page.result_no_face()
            print('text 1:', title1)
            assert title1 == "Total Person", "Total Person text not display"

            print('text 2:', number)
            assert number == "1", "Detect Non Face as Person"
            #ini seharusnya nol, bug di sisi model AI

            print('text 3:', title2)
            assert title2 == "Identified Person", "Identified Person text not display" 

            print('text 4:', number_identified)
            assert number_identified == "1", "Detect Unrecognize as Recognized"

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

#SKIP TEST FR WITH STATUS = PROCESSING

#   Verify "Show Detail" button working well
    @TestTemplate.test_func()
    def test_C7970(self):
        try:
            Login(self._driver).test_C1606()
            FaceRecognition(self._driver).test_C4128()
            fr_page = FaceRecognition_Page(self._driver)

            fr_page.check_result()
            time.sleep(2)
            
            fr_page.click_show_detail()
 
            title = fr_page.title_detail_fr()
            assert title == "Person Detail - Face Recognition", "Not Show title detail popup"
            #sometimes suka error karena nilai div berubah-ubah
            
            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

#   Verify "Go to Enrollment page" text works
    @TestTemplate.test_func()
    def test_C10320(self):
        try:
            Login(self._driver).test_C1606()
            FaceRecognition(self._driver).test_C4128()
            fr_page = FaceRecognition_Page(self._driver)
            enroll_page = EnrollmentPage(self._driver)

            fr_page.check_result()
            time.sleep(2)
            
            fr_page.click_show_detail()

            text = fr_page.go_to_enrollment()
            assert text == "Go to enrollment page", "Not Show Go to enrollment text"
            #sometimes suka error karena nilai div berubah-ubah
            
            title = enroll_page.title_enrollment_xpath()
            assert title == "Upload Complete", "Not Show title of enrollment popup"

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

#   Close Person Detail Popup
    @TestTemplate.test_func()
    def test_C7972(self):
        try:
            Login(self._driver).test_C1606()
            FaceRecognition(self._driver).test_C4128()
            fr_page = FaceRecognition_Page(self._driver)

            fr_page.check_result()
            time.sleep(2)
            
            fr_page.click_show_detail()
            fr_page.close_detail()

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

#skip Face Recognition - Remove "Go to Enrollment page" text until case enrollment and delete success


