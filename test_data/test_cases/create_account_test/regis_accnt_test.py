from test_data.src.pages.create_accnt.regis_page import RegisPage
from test_data.src.pages.login.login_page import LoginPage
from test_data.test_cases.test_data import const
from test_data.test_cases.utils.assert_element import AssertionElement
from skeleton.test_template import TestTemplate
from selenium.webdriver.common.by import By
import time

from faker import Faker

class CreateAccount(TestTemplate):

    def __init__(self, driver):
        super().__init__(const.DOMAIN, driver)

    # direct to register page
    @TestTemplate.test_func()
    def test_C7324(self):
        try:
            regis_page = RegisPage(self._driver)

            regis_page.click_create_account()
            time.sleep(3)

            title_page = regis_page.title_regis_page()
            assert title_page == "Create a Nodeflux Cloud account", "Not Create New Account Page"

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    # Show warning message if Server error 
    @TestTemplate.test_func()
    def test_C6618(self):
        try:
            regis_page = RegisPage(self._driver)
            assert_element = AssertionElement(self._driver)

            fake_data = Faker()
            email = fake_data.safe_email()

            regis_page.click_create_account()
            regis_page.input_email(email)
            regis_page.input_password(const.PASSWORD)
            regis_page.input_confm_password(const.PASSWORD)
            regis_page.click_regis_button()
            time.sleep(1)

            server_error = regis_page.check_server_error()
            assert server_error == "The server did'nt respond", "Message Server didn't respond not display"

            assert_element.is_visible(regis_page.cancel_btn_xpath)
            assert_element.is_visible(regis_page.try_again_btn_xpath)

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    # Verify "Try again" button working well
    @TestTemplate.test_func()
    def test_C6660(self):
        try:
            regis_page = RegisPage(self._driver)

            fake_data = Faker()
            email = fake_data.safe_email()

            regis_page.click_create_account()
            regis_page.input_email(email)
            regis_page.input_password(const.PASSWORD)
            regis_page.input_confm_password(const.PASSWORD)
            regis_page.click_regis_button()
            time.sleep(1)

            server_error = regis_page.check_server_error()
            assert server_error == "The server did'nt respond", "Message Server didn't respond not display"

            regis_page.click_try_again_btn()
            # notes : after click try again button automatically continue regis process (if server is Turn On)

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    # Verify Cancel button working well
    @TestTemplate.test_func()
    def test_C6661(self):
        try:
            regis_page = RegisPage(self._driver)

            fake_data = Faker()
            email = fake_data.safe_email()

            regis_page.click_create_account()
            regis_page.input_email(email)
            regis_page.input_password(const.PASSWORD)
            regis_page.input_confm_password(const.PASSWORD)
            regis_page.click_regis_button()
            time.sleep(1)

            server_error = regis_page.check_server_error()
            assert server_error == "The server did'nt respond", "Message Server didn't respond not display"

            regis_page.click_cancel_btn()

            back_regis_form = regis_page.title_regis_page()
            assert back_regis_form == "Create a Nodeflux Cloud account", "Failed back to Create New Account Page"

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    # Registration success
    @TestTemplate.test_func()
    def test_C1613(self):
        try:
            regis_page = RegisPage(self._driver)

            fake_data = Faker()
            email = fake_data.safe_email()

            regis_page.click_create_account()
            regis_page.input_email(email)
            regis_page.input_password(const.PASSWORD)
            regis_page.input_confm_password(const.PASSWORD)
            regis_page.click_regis_button()
            time.sleep(10)

            success_regis_msg = regis_page.title_success_page()
            assert success_regis_msg == "Your Account has been registered", "Regis success message not display"
            
            regis_page.check_Go_to_Login_button()

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    # Verify "Go to Login Page" [after registered account]
    @TestTemplate.test_func()
    def test_C6658(self):
        try:
            regis_page = RegisPage(self._driver)
            login_page = LoginPage(self._driver)

            fake_data = Faker()
            email = fake_data.safe_email()

            regis_page.click_create_account()
            regis_page.input_email(email)
            regis_page.input_password(const.PASSWORD)
            regis_page.input_confm_password(const.PASSWORD)

            regis_page.click_regis_button()
            time.sleep(10)

            success_regis_msg = regis_page.title_success_page()
            assert success_regis_msg == "Your Account has been registered", "Regis success message not show"

            regis_page.click_Go_to_Login_button()
            time.sleep(1)
            back_login_page = login_page.check_login_page()
            assert back_login_page == "Sign in to continue to Nodeflux Cloud", "Not Login page"

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    # Verify "Sign in to an existing account" text working well
    @TestTemplate.test_func()
    def test_C1619(self):
        try:
            login_page = LoginPage(self._driver)
            regis_page = RegisPage(self._driver)

            regis_page.click_create_account()
            time.sleep(1)
            regis_page.click_sign_in_text()
            time.sleep(1)
            
            back_login_page = login_page.check_login_page()
            assert back_login_page == "Sign in to continue to Nodeflux Cloud", "Not Login page"

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    # Email has been registered
    @TestTemplate.test_func()
    def test_C1637(self):
        try:
            regis_page = RegisPage(self._driver)

            regis_page.click_create_account()
            regis_page.input_email(const.EMAIL)
            regis_page.input_password(const.PASSWORD)
            regis_page.input_confm_password(const.PASSWORD)

            regis_page.click_regis_button()
            time.sleep(3)
            
            email_already_used = regis_page.check_email_error_msg()
            assert email_already_used == "Email is already used please choose another email", "invalid error message"

            regis_btn_status = regis_page.check_regis_btn_status()
            print(regis_btn_status)
            
            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    # Invalid Email format
    @TestTemplate.test_func()
    def test_C1615(self):
        try:
            regis_page = RegisPage(self._driver)

            regis_page.click_create_account()
            regis_page.input_email(const.INVALID_EMAIL)
            regis_page.input_password(const.PASSWORD)
            regis_page.input_confm_password(const.PASSWORD)
            time.sleep(3)
            
            invalid_email_format = regis_page.check_email_error_msg()
            assert invalid_email_format == "Please enter a valid email address", "invalid error message"

            regis_btn_status = regis_page.check_regis_btn_status()
            print(regis_btn_status)

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    # Account name has been registered
    @TestTemplate.test_func()
    def test_C1638(self):
        try:
            regis_page = RegisPage(self._driver)

            regis_page.click_create_account()
            regis_page.input_email(const.EMAIL3)
            regis_page.input_password(const.PASSWORD)
            regis_page.input_confm_password(const.PASSWORD)

            regis_page.click_regis_button()
            time.sleep(3)

            email_already_used = regis_page.check_email_error_msg()
            assert email_already_used == "Email is already used please choose another email", "invalid error message"
            
            accnt_nm_already_used = regis_page.check_accnt_nm_error_msg()
            assert accnt_nm_already_used == "Username is already used please choose another username", "invalid error message"

            regis_btn_status = regis_page.check_regis_btn_status()
            print(regis_btn_status)
            
            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    # Invalid Account Name format 
    @TestTemplate.test_func()
    def test_C1616(self):
        try:
            regis_page = RegisPage(self._driver)

            regis_page.click_create_account()
            regis_page.input_email(const.EMAIL)
            regis_page.input_accnt_name(const.INVALID_ACNT_NAME_FORMAT)
            regis_page.input_password(const.PASSWORD)
            regis_page.input_confm_password(const.PASSWORD)
            time.sleep(3)
            
            invalid_accnt_nm_format = regis_page.check_accnt_nm_error_msg()
            assert invalid_accnt_nm_format == "Only letters, numbers, and ‘_’ with min 5 characters", "invalid error message"

            regis_btn_status = regis_page.check_regis_btn_status()
            print(regis_btn_status)
            
            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    # Input Account name with less than 5 characters
    @TestTemplate.test_func()
    def test_C1731(self):
        try:
            regis_page = RegisPage(self._driver)

            regis_page.click_create_account()
            regis_page.input_email(const.EMAIL)
            regis_page.input_accnt_name(const.INVALID_ACNT_NAME)
            regis_page.input_password(const.PASSWORD)
            regis_page.input_confm_password(const.PASSWORD)
            time.sleep(3)
            
            invalid_accnt_nm = regis_page.check_accnt_nm_error_msg()
            assert invalid_accnt_nm == "Only letters, numbers, and ‘_’ with min 5 characters", "invalid error message"

            regis_btn_status = regis_page.check_regis_btn_status()
            print(regis_btn_status)
            
            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    # Input password with less than 6 characters
    @TestTemplate.test_func()
    def test_C1617(self):
        try:
            regis_page = RegisPage(self._driver)

            regis_page.click_create_account()
            regis_page.input_email(const.EMAIL)
            regis_page.input_password(const.INVALID_PASSWORD_FORMAT)
            time.sleep(3)
            
            invalid_password_format = regis_page.check_password_error_msg()
            assert invalid_password_format == "Please use 6 characters or more", "invalid error message"

            regis_btn_status = regis_page.check_regis_btn_status()
            print(regis_btn_status)
            
            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    # Input invalid Confirm Password
    @TestTemplate.test_func()
    def test_C1618(self):
        try:
            regis_page = RegisPage(self._driver)

            regis_page.click_create_account()
            regis_page.input_email(const.EMAIL)
            regis_page.input_password(const.PASSWORD)
            regis_page.input_confm_password(const.INVALID_CONF_PASSWORD)
            time.sleep(3)
            
            invalid_confirm_password = regis_page.check_conf_password_error_msg()
            assert invalid_confirm_password == "Your password and confirmation password do not match", "invalid error message"

            regis_btn_status = regis_page.check_regis_btn_status()
            print(regis_btn_status)
            
            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    # Hover information symbol beside account name
    @TestTemplate.test_func()
    def test_C1620(self):
        try:
            regis_page = RegisPage(self._driver)

            regis_page.click_create_account()
            regis_page.check_information_icon()
            time.sleep(3)
            
            info_icon = regis_page.check_tooltip_msg()
            assert info_icon == "Only use letters, number, and ‘_’ .\nYou can change it anytime", "invalid error message"
            
            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    # Verify "Show" icon work [on password placeholder]
    @TestTemplate.test_func()
    def test_C6622(self):
        try:
            regis_page = RegisPage(self._driver)

            regis_page.click_create_account()
            regis_page.input_email(const.EMAIL)
            regis_page.input_password(const.PASSWORD)
            regis_page.click_password_icon()
            time.sleep(1)

            password_value = regis_page.check_password_value()
            assert password_value == const.PASSWORD, "wrong password"
            
            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    # Verify "Hide" icon work [on password placeholder]
    @TestTemplate.test_func()
    def test_C7391(self):
        try:
            regis_page = RegisPage(self._driver)
            
            regis_page.click_create_account()
            regis_page.input_email(const.EMAIL)
            regis_page.input_password(const.PASSWORD)
            regis_page.click_password_icon()
            time.sleep(1)
            regis_page.click_password_icon()
            time.sleep(1)

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

     # Verify "Show" icon work [on confirm password placeholder]
    @TestTemplate.test_func()
    def test_C6659(self):
        try:
            regis_page = RegisPage(self._driver)

            regis_page.click_create_account()
            regis_page.input_email(const.EMAIL)
            regis_page.input_password(const.PASSWORD)
            regis_page.input_confm_password(const.PASSWORD)
            regis_page.click_confm_password_icon()
            time.sleep(1)

            confm_password_value = regis_page.check_confirm_password_value()
            assert confm_password_value == const.PASSWORD, "wrong password"
            
            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    # Verify "Hide" icon work [on password placeholder]
    @TestTemplate.test_func()
    def test_C7392(self):
        try:
            regis_page = RegisPage(self._driver)

            regis_page.click_create_account()
            regis_page.input_email(const.EMAIL)
            regis_page.input_password(const.PASSWORD)
            regis_page.input_confm_password(const.PASSWORD)
            regis_page.click_confm_password_icon()
            time.sleep(1)
            regis_page.click_confm_password_icon()
            time.sleep(1)

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    # Verify "nodeflux cloud" icon working well
    @TestTemplate.test_func()
    def test_C7959(self):
        try:
            regis_page = RegisPage(self._driver)
            login_page = LoginPage(self._driver)

            regis_page.click_create_account()
            time.sleep(1)
            regis_page.click_nodeflux_cloud_icon()
            time.sleep(1)

            back_login_page = login_page.check_login_page()
            assert back_login_page == "Sign in to continue to Nodeflux Cloud", "not login page"

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc