from test_data.src.pages.dashboard.dashboard_page import DashboardPage
from test_data.src.pages.login.login_page import LoginPage
from test_data.src.pages.forgot_password.forgot_page import ForgotPasswordPage
from test_data.test_cases.test_data import const
from test_data.test_cases.utils.assert_element import AssertionElement
from skeleton.test_template import TestTemplate
from selenium.webdriver.common.by import By
import time

class Login(TestTemplate):

    def __init__(self, driver):
        super().__init__(const.DOMAIN, driver)

    #Leave all field blank
    @TestTemplate.test_func()
    def test_C3479(self):
        try:
            login_page = LoginPage(self._driver)

            login_page.input_email("")
            login_page.input_password("")
            login_page.click_login_button()
            time.sleep(3)

            userIdentity = login_page.check_error_msg()
            assert userIdentity == "UserIdentity is Required!", "invalid error message"

            return True, None
        except BaseException as exc:
            return False, exc

    #Success with Email Address
    @TestTemplate.test_func()
    def test_C1606(self):
        try:
            login_page = LoginPage(self._driver)
            home_page = DashboardPage(self._driver)
            assert_element = AssertionElement(self._driver)

            login_page.input_email(const.EMAIL) 
            login_page.input_password(const.PASSWORD)
            login_page.click_login_button()
            time.sleep(3)
            assert_element.is_visible(home_page.dashboard_sidebar_name)

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    # Success with Account Name
    @TestTemplate.test_func()
    def test_C4157(self):
        try:
            login_page = LoginPage(self._driver)
            home_page = DashboardPage(self._driver)
            assert_element = AssertionElement(self._driver)

            login_page.input_email(const.ACNT_NAME)
            login_page.input_password(const.PASSWORD)
            login_page.click_login_button()
            assert_element.is_visible(home_page.dashboard_sidebar_name)

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    #Without Activate Account
    @TestTemplate.test_func()
    def test_C6655(self):
        try:
            login_page = LoginPage(self._driver)

            login_page.input_email(const.UNVERIFIED_EMAIL)
            login_page.input_password(const.PASSWORD)
            login_page.click_login_button()
            time.sleep(10)

            activation_emsg = login_page.check_activation_emsg()
            assert activation_emsg == "We have resent you an activation link to your email, please activate your account first.", "not show popup modal for activate account"
            login_page.click_close_button()

            msg_confirm_email = login_page.check_error_msg()
            assert msg_confirm_email == "Please confirmed The email you entered", "invalid error message"
            #not check email received

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    #Verify Close button working well
    @TestTemplate.test_func()
    def test_C6657(self):
        try:
            login_page = LoginPage(self._driver)

            login_page.input_email(const.UNVERIFIED_EMAIL)
            login_page.input_password(const.PASSWORD)
            login_page.click_login_button()
            time.sleep(10)
            login_page.click_close_button()

            login_page = login_page.check_login_page()
            assert login_page == "Sign in to continue to Nodeflux Cloud", "not back to login page"

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    #Verify "Show" icon work [on password placeholder]
    @TestTemplate.test_func()
    def test_C6621(self):
        try:
            login_page = LoginPage(self._driver)

            login_page.input_email(const.EMAIL)
            login_page.input_password(const.PASSWORD)
            login_page.click_password_icon()
            time.sleep(1)

            password_value = login_page.check_password_value()
            assert password_value == const.PASSWORD, "wrong password"

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    #Verify "Hide" icon work [on password placeholder]
    @TestTemplate.test_func()
    def test_C1636(self):
        try:
            login_page = LoginPage(self._driver)

            login_page.input_email(const.EMAIL)
            login_page.input_password(const.PASSWORD)
            login_page.click_password_icon()
            time.sleep(1)
            login_page.click_password_icon()
            time.sleep(1)

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    #Invalid Email format
    @TestTemplate.test_func()
    def test_C7958(self):
        try:
            login_page = LoginPage(self._driver)

            login_page.input_email(const.INVALID_EMAIL)
            login_page.input_password(const.PASSWORD)
            login_page.click_login_button()
            time.sleep(3)

            invalid_email_format = login_page.check_error_msg()
            assert invalid_email_format == "The email/password you entered is incorrect", "invalid error message"

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    #with wrong email address
    @TestTemplate.test_func()
    def test_C1594(self):
        try:
            login_page = LoginPage(self._driver)

            login_page.input_email(const.WRONG_EMAIL)
            login_page.input_password(const.PASSWORD)
            login_page.click_login_button()
            time.sleep(3)

            wrong_email = login_page.check_error_msg()
            assert wrong_email == "The email/password you entered is incorrect", "invalid error message"

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    #with wrong account name
    @TestTemplate.test_func()
    def test_C1803(self):
        try:
            login_page = LoginPage(self._driver)

            login_page.input_email(const.WRONG_ACNT_NAME)
            login_page.input_password(const.PASSWORD)
            login_page.click_login_button()
            time.sleep(3)

            wrong_accnt_name = login_page.check_error_msg()
            assert wrong_accnt_name == "The email/password you entered is incorrect", "invalid error message"

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    # without input password
    @TestTemplate.test_func()
    def test_C6811(self):
        try:
            login_page = LoginPage(self._driver)

            login_page.input_email(const.EMAIL)
            login_page.input_password("")
            login_page.click_login_button()
            time.sleep(3)

            password_required = login_page.check_error_msg()
            assert password_required == "Password is Required!", "invalid error message"

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    # with wrong password [by email]
    @TestTemplate.test_func()
    def test_C1607(self):
        try:
            login_page = LoginPage(self._driver)

            login_page.input_email(const.EMAIL)
            login_page.input_password(const.INVALID_PASSWORD)
            login_page.click_login_button()
            time.sleep(3)

            invalid_password_e = login_page.check_error_msg()
            assert invalid_password_e == "The email/password you entered is incorrect", "invalid error message"

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc
    
    # with wrong password [by account Name]
    @TestTemplate.test_func()
    def test_C6620(self):
        try:
            login_page = LoginPage(self._driver)

            login_page.input_email(const.ACNT_NAME)
            login_page.input_password(const.INVALID_PASSWORD)
            login_page.click_login_button()
            time.sleep(3)

            invalid_password_a = login_page.check_error_msg()
            assert invalid_password_a == "The email/password you entered is incorrect", "invalid error message"

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    # Sign in with Google Account
    @TestTemplate.test_func()
    def test_C6844(self):
        try:
            login_page = LoginPage(self._driver)
            dashboard_page = DashboardPage(self._driver)
            assert_element = AssertionElement(self._driver)

            login_page.click_sign_in_google_accnt()
            login_page.input_google_accnt(const.REGIS_GOOGLE_ACNT)
            login_page.click_next_button()
            time.sleep(3)
            login_page.input_password_google_acnt(const.PASSWORD_GOOGLE_ACNT)
            time.sleep(3)
            assert_element.is_visible(dashboard_page.dashboard_sidebar_name)
            #not regis with google acnt, because Google denied automate

            accnt_activated = dashboard_page.msg_activated()
            assert accnt_activated == "Account Successfully Activated", "Account message activated not display"

            click_here_text = dashboard_page.check_click_here_text()
            assert click_here_text == "Click here", "Text Click Here not display"

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    # Login again with Google Account
    @TestTemplate.test_func()
    def test_C1611(self):
        try:
            login_page = LoginPage(self._driver)
            home_page = DashboardPage(self._driver)
            assert_element = AssertionElement(self._driver)

            login_page.click_sign_in_google_accnt()
            login_page.input_google_accnt(const.LOGIN_BYGOOGLE)
            login_page.click_next_button()
            time.sleep(5)
            login_page.input_password_google_acnt(const.PASSWORD_GOOGLE_ACNT)
            time.sleep(5)
            assert_element.is_visible(home_page.dashboard_sidebar_name)
            time.sleep(8)
            #can't login with google account , because Google denied automate
            
            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    # with invalid Google Account
    @TestTemplate.test_func()
    def test_C6646(self):
        try:
            login_page = LoginPage(self._driver)

            login_page.click_sign_in_google_accnt()
            login_page.input_google_accnt(const.WRONG_EMAIL)
            login_page.click_next_button()
            time.sleep(5)

            invalid_google_acnt = login_page.error_invalid_google_accnt
            assert invalid_google_acnt == "Tidak dapat menemukan Akun Google Anda", "invalid error message"

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    # Verify "nodeflux cloud" icon working well
    @TestTemplate.test_func()
    def test_C6654(self):
        try:
            forgot_password_page = ForgotPasswordPage(self._driver)
            login_page = LoginPage(self._driver)
            
            forgot_password_page.click_forgot_password()
            time.sleep(1)
            login_page.click_nodeflux_cloud_icon()
            time.sleep(1)

            back_login_page = login_page.check_login_page()
            assert back_login_page == "Sign in to continue to Nodeflux Cloud", "not login page"

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc
    
