from os import getcwd

# Data Login Cloud
DOMAIN = "https://cloud.nodeflux.io/"
DOMAIN_STG = "https://stg.cloud.nodeflux.io/"
# BILLING_URL = "https://stg.cloud.nodeflux.io/dashboard/billing"

EMAIL = "qacloud15@gmail.com"
INVALID_EMAIL = "frisca@@nodeflux.io"
ACNT_NAME = "MemberCloud"
PASSWORD = "test1234*"

UNVERIFIED_EMAIL = "testersa9@gmail.com" 
WRONG_EMAIL = "frisca2@nodeflux.io" 
WRONG_ACNT_NAME = "Stg_frisca"
INVALID_PASSWORD = "TEST1234*"

REGIS_GOOGLE_ACNT = "anyatest12@gmail.com" #have not been used
PASSWORD_GOOGLE_ACNT = "Arianja*8"
LOGIN_BYGOOGLE = "qacloud17@gmail.com"

EMAIL2 = "testercloud12@gmail.com" 
EMAIL3 = "qacloud15@gmail.com" 
INVALID_ACNT_NAME_FORMAT = "Fr*s Ar*anja"
INVALID_ACNT_NAME = "PT"
INVALID_PASSWORD_FORMAT = 'Fris'
INVALID_CONF_PASSWORD = 'NotMatch123'

STATUS_QUICK_SEARCH = 'PROCESSING'

PATH_FDEMO_IMG = getcwd()+'/test_data/test_cases/utils/images/face_demography/three_people.jpg'
PATH_FRECOG_IMG = getcwd()+'/test_data/test_cases/utils/images/face_recognition/know&unknown.jpg'
PATH_FMATCH1_IMG = getcwd()+'/test_data/test_cases/utils/images/face_match/anang_face.jpg'
PATH_FMATCH2_IMG = getcwd()+'/test_data/test_cases/utils/images/face_match/anang.jpg'
PATH_LPR_IMG1 = getcwd()+'/test_data/test_cases/utils/images/lpr/two_car.jpeg'
PATH_LPR_IMG2 = getcwd()+'/test_data/test_cases/utils/images/lpr/black_car.jpg'
PATH_LPR_IMG3 = getcwd()+'/test_data/test_cases/utils/images/lpr/car.jpeg'
PATH_PLANOGRAM = getcwd()+'/test_data/test_cases/utils/images/planogram/fit01.jpg'

PATH_NO_FACE = getcwd()+'/test_data/test_cases/utils/images/negative_test/no_face.jpg' 
PATH_NO_HUMAN_FACE = getcwd()+'/test_data/test_cases/utils/images/negative_test/monkey.jpg' 
PATH_IMG_IS_MORE_SIZE = getcwd()+'/test_data/test_cases/utils/images/negative_test/more_max_size.jpg'
PATH_VALID_IMG = getcwd()+'/test_data/test_cases/utils/images/negative_test/valid_photo.jpg'
PATH_PNG_RENAME = getcwd()+'/test_data/test_cases/utils/images/negative_test/png_rename.jpg'
