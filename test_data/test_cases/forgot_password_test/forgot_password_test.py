from test_data.src.pages.forgot_password.forgot_page import ForgotPasswordPage
from test_data.test_cases.test_data import const
from test_data.test_cases.utils.assert_element import AssertionElement
from skeleton.test_template import TestTemplate
from selenium.webdriver.common.by import By
import time

class ForgotPassword(TestTemplate):

    def __init__(self, driver):
        super().__init__(const.DOMAIN, driver)

    #direct to "forgot password" page
    @TestTemplate.test_func()
    def test_C1608(self):
        try:
            forgot_password_page = ForgotPasswordPage(self._driver)

            forgot_password_page.click_forgot_password()

            title_page = forgot_password_page.title_page()
            assert title_page == "Forgot your password ?", "Not Forgot Password Page"

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    #Leave email address field blank
    @TestTemplate.test_func()
    def test_C6647(self):
        try:
            forgot_password_page = ForgotPasswordPage(self._driver)

            forgot_password_page.click_forgot_password()
            time.sleep(2)

            forgot_password_page.check_submit_button_status()

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

    #Email is not registered [Manual Test]
    @TestTemplate.test_func()
    def test_C1609(self):
        try:
            forgot_password_page = ForgotPasswordPage(self._driver)

            forgot_password_page.click_forgot_password()
            forgot_password_page.input_email(const.WRONG_EMAIL)
            time.sleep(3)
            forgot_password_page.click_checkbox_recaptcha()
            #not click recaptcha checkbox, because recaptcha question can't automate

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc
    
    #Invalid Email format
    @TestTemplate.test_func()
    def test_C1635(self):
        try:
            forgot_password_page = ForgotPasswordPage(self._driver)
            
            forgot_password_page.click_forgot_password()
            forgot_password_page.input_email(const.INVALID_EMAIL)
            time.sleep(3)
            
            invalid_email = forgot_password_page.check_error_msg()
            assert invalid_email == "Please enter a valid email address", "invalid error message"

            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc