from test_data.src.pages.dashboard.dashboard_page import DashboardPage
from test_data.src.pages.login.login_page import LoginPage
from test_data.src.pages.billing.biling_page import BillingPage
from test_data.test_cases.test_data import const
from test_data.test_cases.utils.assert_element import AssertionElement
from skeleton.test_template import TestTemplate
from selenium.webdriver.common.by import By
from test_data.test_cases.login_test.login_test import Login
from test_data.src.pages.profile.profile_page import ProfilePage
import time
from selenium.webdriver.common.action_chains import ActionChains

class Dashboard(TestTemplate):

    def __init__(self, driver):
        super().__init__(const.DOMAIN, driver)

# Show message "Account Successfully Activated"
    @TestTemplate.test_func()
    def test_C1734(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)

            accnt_activated_msg = dashboard_page.msg_activated()
            assert accnt_activated_msg == "Account Successfully Activated", "Account activated message not display"

            click_here_text = dashboard_page.check_click_here_text()
            assert click_here_text == "Click here", "Click Here text not display"

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Verify "Click here" text working well
    @TestTemplate.test_func()
    def test_C6651(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
            profile_page = ProfilePage(self._driver)
            assert_element = AssertionElement(self._driver)

            accnt_activated_msg = dashboard_page.msg_activated()
            assert accnt_activated_msg == "Account Successfully Activated", "Account activated message not display"
          
            dashboard_page.click_here()
            time.sleep(2)
            assert_element.is_visible(profile_page.profile_title_xpath)

            usage_quota_title = profile_page.usage_quota_title()
            assert usage_quota_title == "Usage Status - Quota", "Usage quota section not display on  profile page"

            check_quota_free = profile_page.check_quota_free()
            assert check_quota_free == "1000/ 1000", "Not get 1000 quota free for first time"

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Show Alert message quota is low
    @TestTemplate.test_func()
    def test_C6927(self):
        try:
            login_page = LoginPage(self._driver)
            dashboard_page = DashboardPage(self._driver)

            login_page.input_email(const.EMAIL3)
            login_page.input_password(const.PASSWORD)
            login_page.click_login_button()
            time.sleep(8)

            self._driver.refresh()
            time.sleep(3)

            msg_quota_low = dashboard_page.quota_msg()
            assert msg_quota_low == "1 Your analytic quota is low", "Message quota is low not display"

            click_here_text = dashboard_page.check_billing_text()
            assert click_here_text == "Check the billing page", "Check the billing page text not display"

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Verify "Check the billing page" text working well [Quota is Low]
    @TestTemplate.test_func()
    def test_C6928(self):
        try:
            login_page = LoginPage(self._driver)
            dashboard_page = DashboardPage(self._driver)
            billing_page = BillingPage(self._driver)

            login_page.input_email(const.EMAIL3)
            login_page.input_password(const.PASSWORD)
            login_page.click_login_button()
            time.sleep(8)

            self._driver.refresh()  
            time.sleep(3)

            msg_quota_low = dashboard_page.quota_msg()
            assert msg_quota_low == "1 Your analytic quota is low", "Message quota is low not display"

            dashboard_page.click_check_billing()
            time.sleep(3)

            check_billing_page = billing_page.check_billing_title()
            assert check_billing_page == "Billing", "Not Billing Page"

            billing_page.check_warning_icon() 
            time.sleep(5)

            info_icon = billing_page.check_tooltip_msg()
            assert info_icon == "The quota for this analytic is running low", "invalid error message"

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Show Alert message quota is empty
    @TestTemplate.test_func()
    def test_C6929(self):
        try:
            login_page = LoginPage(self._driver)
            dashboard_page = DashboardPage(self._driver)

            login_page.input_email(const.EMAIL2)
            login_page.input_password(const.PASSWORD)
            login_page.click_login_button()
            time.sleep(3)

            self._driver.refresh()
            time.sleep(3)

            msg_quota_low = dashboard_page.quota_msg()
            assert msg_quota_low == "4 Your analytic quota is low", "Message quota is empty not display"

            click_here_text = dashboard_page.check_billing_text()
            assert click_here_text == "Check the billing page", "Check the billing page text not display"

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Verify "Check the billing page" working well [Quota is Empty]
    @TestTemplate.test_func()
    def test_C6944(self):
        try:
            login_page = LoginPage(self._driver)
            dashboard_page = DashboardPage(self._driver)
            billing_page = BillingPage(self._driver)

            login_page.input_email(const.EMAIL2)
            login_page.input_password(const.PASSWORD)
            login_page.click_login_button()
            time.sleep(3)

            self._driver.refresh()
            time.sleep(5)

            msg_quota_low = dashboard_page.quota_msg()
            assert msg_quota_low == "4 Your analytic quota is low", "Message quota is empty not display"

            dashboard_page.click_check_billing()
            time.sleep(2)

            check_billing_page = billing_page.check_billing_title()
            assert check_billing_page == "Billing", "Not Billing Page"
          
            billing_page.check_warning_icon() 
            time.sleep(3)

            info_icon = billing_page.check_tooltip_msg()
            assert info_icon == "The quota for this analytic is running low", "invalid error message"


            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Verify Dashboard is on menu list
    @TestTemplate.test_func()
    def test_C10365(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
            
            dashboard_mtitle = dashboard_page.check_dashboard_menu()
            assert dashboard_mtitle == "DASHBOARD", "Not Dashboard Menu"

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Verify Dashboard Menu working well
    @TestTemplate.test_func()
    def test_C6812(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
            billing_page = BillingPage(self._driver)

            billing_page.click_billing_menu()
            time.sleep(1)

            dashboard_page = dashboard_page.click_dashboard_menu()
            assert dashboard_page == "Dashboard", "Not Dashboard Page"

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Verify Detection List Tab working well
    @TestTemplate.test_func()
    def test_C10364(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)

            dashboard_page.click_dashboard_menu()
            
            detection_list_page = dashboard_page.click_detection_list()
            assert detection_list_page == "DETECTION LIST", "Not Detection list Page"

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

#   Verify Scrollbar working well
    @TestTemplate.test_func()
    def test_C22645(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)

            dashboard_page.click_dashboard_menu()
            
            detection_list_page = dashboard_page.click_detection_list()
            assert detection_list_page == "DETECTION LIST", "Not Detection list Page"

            dashboard_page.scrolling()
            time.sleep(1)
            
            return True, None
        except BaseException as exc:
            print(exc)
            return False, exc

# Quick Search Status
    # @TestTemplate.test_func()
    # def test_C1653(self):
    #     try:
    #         Login(self._driver).test_C1606()
    #         dashboard_page = DashboardPage(self._driver)
    #         dashboard_page.click_dashboard_menu()         
    #         dashboard_page.click_detection_list()
    #         dashboard_page.input_quick_search(const.STATUS_QUICK_SEARCH)
    #         time.sleep(3)

    #         quick_search_status = dashboard_page.find_status_processing()
    #         assert quick_search_status == "PROCESSING", "Not found detection list status = PROCESSING"

    #         return True, None
    #     except BaseException as exc:
    #         print("exc", exc)
    #         return False, exc
    # hold because quick_search element hide for current condition

# Sort By Analytic
    @TestTemplate.test_func()
    def test_C1654(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
         
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            
            sort_by_title = dashboard_page.click_sort_by_button()
            assert sort_by_title == "Sort By", "Sort By text not display"
            time.sleep(1)
            title = dashboard_page.choose_sortby_analytic()
            assert title == "Analytic", "Analytic option not display"
            time.sleep(3)
            dashboard_page.click_arrow_symbol()
            time.sleep(3)

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Sort By Date
    @TestTemplate.test_func()
    def test_C1655(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
         
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            
            sort_by_title = dashboard_page.click_sort_by_button()
            assert sort_by_title == "Sort By", "Sort By text not display"
            time.sleep(3)
            title = dashboard_page.choose_sortby_date()
            assert title == "Date", "Date option not display"
            dashboard_page.click_arrow_symbol()
            time.sleep(3)

            result_date_sorting = dashboard_page.check_date_sorting()
            assert result_date_sorting == "29 Aug, 14:58:38", "Not sorting from oldest date"

            dashboard_page.click_arrow_symbol()
            time.sleep(3)

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Sort By Status
    @TestTemplate.test_func()
    def test_C1656(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
         
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            
            sort_by_title = dashboard_page.click_sort_by_button()
            assert sort_by_title == "Sort By", "Sort By text not display"
            time.sleep(1)
            title = dashboard_page.choose_sortby_status()
            assert title == "Status", "Status option not display"
            time.sleep(3)

            result_status_sorting = dashboard_page.check_status_sorting()
            assert result_status_sorting == "SUCCESS", "Not sorting by descending status"
            
            dashboard_page.click_arrow_symbol()
            time.sleep(3)

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Filter by One Date
    @TestTemplate.test_func()
    def test_C1658(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
         
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            
            filter_title = dashboard_page.check_filter_text()
            assert filter_title == "Filter", "Filter text not display"

            dashboard_page.choose_one_time()

            dashboard_page.click_date_from()
            dashboard_page.click_prev_month()
            dashboard_page.choose_date_from()
            dashboard_page.close_calender()
            time.sleep(1)
            dashboard_page.close_filter_popup()
            time.sleep(4)

            result_filter = dashboard_page.check_result_onedt()
            assert result_filter == "27 Jan, 20:25:01", "Data not found"
            #hit to analytic every second day on week 5 and change the date  
            
            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Filter by Range Date
    @TestTemplate.test_func()
    def test_C1796(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
         
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()

            filter_title = dashboard_page.check_filter_text()
            assert filter_title == "Filter", "Filter text not display"

            dashboard_page.choose_range_time()

            dashboard_page.click_date_from()
            dashboard_page.click_prev_month()
            dashboard_page.choose_date_from()
            time.sleep(3)
            dashboard_page.close_calender()
            time.sleep(1)

            dashboard_page.click_date_to()
            dashboard_page.click_next_month()
            dashboard_page.choose_date_to()
            time.sleep(3)
            dashboard_page.close_calender()
            time.sleep(1)

            dashboard_page.close_filter_popup()
            time.sleep(1)

            result_filter_1 = dashboard_page.check_result_rangedt()
            assert result_filter_1 == "03 Feb, 17:12:08", "Data not found"

            # dashboard_page.next_result()
            # time.sleep(1)

            # result_filter_2 = dashboard_page.check_result_rangedt()
            # assert result_filter_2 == "27 Jan, 20:25:01", "Data not found"

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Filter by Features
    @TestTemplate.test_func()
    def test_C1659(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
         
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            
            filter_title = dashboard_page.check_filter_text()
            assert filter_title == "Filter", "Filter text not display"
         
            fdemo = dashboard_page.choose_face_demo()
            assert fdemo == "Face Demography API", "Not found Face Demography option"

            frecog = dashboard_page.choose_face_recog()
            assert frecog == "Face Recognition API", "Not found Face Recognition option"

            dashboard_page.close_filter_popup()
            time.sleep(1)

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Filter by Status
    @TestTemplate.test_func()
    def test_C1660(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
         
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            
            filter_title = dashboard_page.check_filter_text()
            assert filter_title == "Filter", "Filter text not display"

            status_text = dashboard_page.choose_processing_status()
            assert status_text == "Processing", "Not found Processing Status option"

            dashboard_page.close_filter_popup()
            time.sleep(1)

            result_status = dashboard_page.find_status_processing()
            assert result_status == "PROCESSING", "Not found detection list status = PROCESSING"

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Filter by Source key  
    @TestTemplate.test_func()
    def test_C6653(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
         
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
           
            filter_title = dashboard_page.check_filter_text()
            assert filter_title == "Filter", "Filter text not display"

            check_key = dashboard_page.choose_access_key()
            assert check_key == "P1SEKRPL56V5T6L7T285BUFRT", "Not found access key"

            dashboard_page.close_filter_popup()
            time.sleep(1)

            result_access_key = dashboard_page.check_result_access_key()
            assert result_access_key == "P1SEKRPL56V5T6L7T285BUFRT", "Not found result filter access key"

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Verify Clear All Filter Button
    @TestTemplate.test_func()
    def test_C1661(self):
        try:
            Login(self._driver).test_C1606()
            Dashboard(self._driver).test_C1659()
            dashboard_page = DashboardPage(self._driver)
            
            filter_title = dashboard_page.check_filter_text()
            assert filter_title == "Filter", "Filter text not display"

            dashboard_page.clear_all_filter()
            time.sleep(1)
            dashboard_page.check_analytic_checkbox_status()

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc
    
# Verify Upload button is on detection list page
    @TestTemplate.test_func()
    def test_C1646(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()

            upload_btn = dashboard_page.check_upload_btn()
            assert upload_btn == "Upload", "Upload button text not display"

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Verify Upload button working well  
    @TestTemplate.test_func()
    def test_C1647(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            
            upload_popup_title = dashboard_page.upload_popup_title()
            assert upload_popup_title == "Drag image file(s) here or\nBrowse from your computer"

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Verify Close button on browse popup box
    @TestTemplate.test_func()
    def test_C1650(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            dashboard_page.close_browse_popup()
            time.sleep(1)

            detection_list_page = dashboard_page.click_detection_list()
            assert detection_list_page == "DETECTION LIST", "Not Detection list Page"

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

#  Verify Change Photo working well  
    @TestTemplate.test_func()
    def test_C1648(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            dashboard_page.input_file(const.PATH_FDEMO_IMG)
            time.sleep(1)
            dashboard_page.click_remove_img()
            time.sleep(1)
            dashboard_page.input_file(const.PATH_FRECOG_IMG)
            time.sleep(1)

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

#  Verify Close button on upload popup box 
    @TestTemplate.test_func()
    def test_C1651(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            dashboard_page.input_file(const.PATH_FDEMO_IMG)
            time.sleep(1)
            dashboard_page.close_upload_popup()
            time.sleep(1)

            detection_list_page = dashboard_page.click_detection_list()
            assert detection_list_page == "DETECTION LIST", "Not Detection list Page"

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Verify Cancel button on upload popup box
    @TestTemplate.test_func()
    def test_C1652(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            dashboard_page.input_file(const.PATH_FDEMO_IMG)
            time.sleep(1)
            dashboard_page.click_cancel_btn()

            detection_list_page = dashboard_page.click_detection_list()
            assert detection_list_page == "DETECTION LIST", "Not Detection list Page"

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Remove all photo uploaded 
    @TestTemplate.test_func()
    def test_C10381(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            dashboard_page.input_file(const.PATH_FDEMO_IMG)
            time.sleep(1)
            
            fd_checkbox = dashboard_page.face_demo_checkbox()
            assert fd_checkbox == "Face Demography", "Face Demography checkbox not display"

            dashboard_page.click_remove_img()
            time.sleep(1)
            dashboard_page.check_done_btn_status()

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Upload 1 valid photo [by Select]
    @TestTemplate.test_func()
    def test_C6873(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            dashboard_page.input_file(const.PATH_FDEMO_IMG)
            time.sleep(1)
            dashboard_page.check_file_uploaded()

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# upload > 1 valid photo [by Select]
    @TestTemplate.test_func()
    def test_C6875(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            dashboard_page.input_file(const.PATH_FDEMO_IMG)
            dashboard_page.input_file(const.PATH_FRECOG_IMG)
            time.sleep(3)
            dashboard_page.check_multifile_uploaded()

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# upload 1 valid photo, others more than max size [by Select]
    @TestTemplate.test_func()
    def test_C3480(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            dashboard_page.input_file(const.PATH_VALID_IMG)
            dashboard_page.input_file(const.PATH_IMG_IS_MORE_SIZE)
            time.sleep(5)
 
            error_msg_validation = dashboard_page.check_error_validation()
            assert error_msg_validation == '"more_max_size.jpg, could not be uploaded. The images is invalid format, or not supported., "', "invalid error message"
            dashboard_page.check_valid_file()

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# upload 1 valid photo and 1 photo renamed to .jpg [by Select]
    @TestTemplate.test_func()
    def test_C10372(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            dashboard_page.input_file(const.PATH_VALID_IMG)
            dashboard_page.input_file(const.PATH_PNG_RENAME)
            time.sleep(1)

            error_msg_validation = dashboard_page.check_error_validation()
            assert error_msg_validation == '"png_rename.jpg, could not be uploaded. The images is invalid format, or not supported., "', "invalid error message"
            dashboard_page.check_valid_file()

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# upload more than Max size [by Select]
    @TestTemplate.test_func()
    def test_C6874(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            dashboard_page.input_file(const.PATH_IMG_IS_MORE_SIZE)
            dashboard_page.input_file(const.PATH_IMG_IS_MORE_SIZE)
            time.sleep(1)

            error_msg_max_size = dashboard_page.check_error_max_size()
            assert error_msg_max_size == "2 Unsupported file type or\nfile size too large", "invalid error message"

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

#  upload photo that has been renamed [by Select]
    @TestTemplate.test_func()
    def test_C10367(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            dashboard_page.input_file(const.PATH_PNG_RENAME)
            dashboard_page.input_file(const.PATH_PNG_RENAME)
            time.sleep(1)

            error_msg_validation = dashboard_page.check_error_validation()
            assert error_msg_validation == '"png_rename.jpg, could not be uploaded. The images is invalid format, or not supported., "', "invalid error message"

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Verify Add Photo button working well
    @TestTemplate.test_func()
    def test_C3478(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            dashboard_page.input_file(const.PATH_FDEMO_IMG)
            time.sleep(1)

            title = dashboard_page.check_add_photo()
            assert title == "Add photo", "invalid error message"

            dashboard_page.input_file(const.PATH_FDEMO_IMG)
            time.sleep(1)
            dashboard_page.check_file_uploaded()

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Verify Done button disable before choose analytic
    @TestTemplate.test_func()
    def test_C4036(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            dashboard_page.input_file(const.PATH_FDEMO_IMG)
            time.sleep(1)
            dashboard_page.check_done_btn_status()

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Verify Planogram checkbox active
    @TestTemplate.test_func()
    def test_C4037(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            dashboard_page.input_file(const.PATH_PLANOGRAM)
            time.sleep(1)

            text = dashboard_page.planogram_checkbox()
            assert text == "Planogram", "planogram checkbox not display"  
            dashboard_page.check_anothers_checkbox()

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

#  Verify Planogram checkbox disable
    @TestTemplate.test_func()
    def test_C1657(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            dashboard_page.input_file(const.PATH_FDEMO_IMG)
            time.sleep(1)
            
            fd_checkbox = dashboard_page.face_demo_checkbox()
            assert fd_checkbox == "Face Demography", "Face Demography checkbox not display"  
            dashboard_page.check_planogram_checkbox()    

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Verify Face Match checkbox disable
    @TestTemplate.test_func()
    def test_C6871(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            dashboard_page.input_file(const.PATH_FDEMO_IMG)
            time.sleep(1)
            
            fd_checkbox = dashboard_page.face_demo_checkbox()
            assert fd_checkbox == "Face Demography", "Face Demography checkbox not display"  
            dashboard_page.check_face_match_checkbox()    

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Verify Face Match checkbox active
    @TestTemplate.test_func()
    def test_C6846(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            dashboard_page.input_file(const.PATH_FMATCH1_IMG)
            dashboard_page.input_file(const.PATH_FMATCH2_IMG)
            time.sleep(1)
            
            fd_checkbox = dashboard_page.face_match_checkbox()
            assert fd_checkbox == "Face Match", "Face Match checkbox not display"    
            dashboard_page.check_anothers_checkbox()

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# Verify Pagination button working well
    @TestTemplate.test_func()
    def test_C4040(self):
        try:
            Login(self._driver).test_C1606()
            dashboard_page = DashboardPage(self._driver)
            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            time.sleep(2)
            dashboard_page.next_result()
            time.sleep(3)

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

#  Verify Disable Checkbox analytic working well
    @TestTemplate.test_func()
    def test_C6818(self):
        try:
            login_page = LoginPage(self._driver)
            dashboard_page = DashboardPage(self._driver)

            login_page.input_email(const.EMAIL2)
            login_page.input_password(const.PASSWORD)
            login_page.click_login_button()
            time.sleep(8)
                        
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            dashboard_page.input_file(const.PATH_LPR_IMG1 + "\n " + const.PATH_LPR_IMG2 + "\n " + const.PATH_LPR_IMG3 + "\n " + const.PATH_LPR_IMG1)
            time.sleep(5)

            lpr_checkbox = dashboard_page.lpr_checkbox1()
            assert lpr_checkbox == "License Plate Recognition (LPR)", "LPR checkbox not display" 

            dashboard_page.check_done_btn_status()

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

#  Hover Warning icon on Checkbox analytic
    @TestTemplate.test_func()
    def test_C7960(self):
        try:
            login_page = LoginPage(self._driver)
            dashboard_page = DashboardPage(self._driver)

            login_page.input_email(const.EMAIL2)
            login_page.input_password(const.PASSWORD)
            login_page.click_login_button()
            time.sleep(8)   

            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            dashboard_page.input_file(const.PATH_LPR_IMG1 + "\n " + const.PATH_LPR_IMG2 + "\n " + const.PATH_LPR_IMG3 + "\n " + const.PATH_LPR_IMG1)
            
            lpr_checkbox = dashboard_page.lpr_checkbox1()
            assert lpr_checkbox == "License Plate Recognition (LPR)", "LPR checkbox not display" 

            dashboard_page.check_warning_checkbox()
            #can't click warning checkbox icon
            msg = dashboard_page.check_tooltip_checkbox()
            assert msg == "Not enough quota available for this analytic.\n Please contact us at hi@nodeflux.io for more information", "Msg Tooltip Checkbox is wrong" 

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

#  Verify Enable Checkbox analytic working well
    @TestTemplate.test_func()
    def test_C6819(self):
        try:
            login_page = LoginPage(self._driver)
            dashboard_page = DashboardPage(self._driver)

            login_page.input_email(const.EMAIL2)
            login_page.input_password(const.PASSWORD)
            login_page.click_login_button()
            time.sleep(3)            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            dashboard_page.input_file(const.PATH_LPR_IMG1 + "\n " + const.PATH_LPR_IMG2 + "\n " + const.PATH_LPR_IMG3 + "\n " + const.PATH_LPR_IMG1)
            
            lpr_checkbox = dashboard_page.lpr_checkbox1()
            assert lpr_checkbox == "License Plate Recognition (LPR)", "LPR checkbox not display" 
            time.sleep(3)
            dashboard_page.click_remove_img()
            time.sleep(3)
            dashboard_page.lpr_checkbox2()

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

#   Verify Add Photo disable [by Drag image]
    @TestTemplate.test_func()
    def test_C6820(self):
        try:
            login_page = LoginPage(self._driver)
            dashboard_page = DashboardPage(self._driver)

            login_page.input_email(const.EMAIL2)
            login_page.input_password(const.PASSWORD)
            login_page.click_login_button()
            time.sleep(3)            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            dashboard_page.input_file(const.PATH_LPR_IMG1 + "\n " + const.PATH_LPR_IMG2 + "\n " + const.PATH_LPR_IMG3)
            
            dashboard_page.click_lpr_checkbox()
            time.sleep(1)
            dashboard_page.check_add_photo()
            dashboard_page.check_add_photo_status1()

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

#   Verify if there are no photos uploaded
    @TestTemplate.test_func()
    def test_C6824(self):
        try:
            login_page = LoginPage(self._driver)
            dashboard_page = DashboardPage(self._driver)

            login_page.input_email(const.EMAIL2)
            login_page.input_password(const.PASSWORD)
            login_page.click_login_button()
            time.sleep(3)            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            dashboard_page.input_file(const.PATH_LPR_IMG1 + "\n " + const.PATH_LPR_IMG2 + "\n " + const.PATH_LPR_IMG3)
            
            dashboard_page.click_lpr_checkbox()
            time.sleep(1)
            dashboard_page.click_remove_img()
            dashboard_page.click_remove_img()
            dashboard_page.click_remove_img()
            time.sleep(1)
            dashboard_page.click_lpr_checkbox()
            time.sleep(1)
            dashboard_page.click_lpr_checkbox()
            time.sleep(1)
            
            error_msg_validation = dashboard_page.check_error_validation()
            assert error_msg_validation == '"add your images, "', "invalid error message"

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

#   Verify Add photo enable [by uploading one by one]
    @TestTemplate.test_func()
    def test_C6849(self):
        try:
            login_page = LoginPage(self._driver)
            dashboard_page = DashboardPage(self._driver)

            login_page.input_email(const.EMAIL2)
            login_page.input_password(const.PASSWORD)
            login_page.click_login_button()
            time.sleep(3)            
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            dashboard_page.input_file(const.PATH_LPR_IMG1 + "\n " + const.PATH_LPR_IMG2 + "\n " + const.PATH_LPR_IMG3)
            
            dashboard_page.click_lpr_checkbox()
            time.sleep(1)
            dashboard_page.check_add_photo()
            dashboard_page.check_add_photo_status1()
            dashboard_page.click_remove_img()
            dashboard_page.check_add_photo()
            dashboard_page.check_add_photo_status2()

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc

# All Checkbox analytics disable
    @TestTemplate.test_func()
    def test_C6817(self):
        try:
            login_page = LoginPage(self._driver)
            dashboard_page = DashboardPage(self._driver)

            login_page.input_email(const.EMAIL2)
            login_page.input_password(const.PASSWORD)
            login_page.click_login_button()
            time.sleep(3)
            dashboard_page.click_dashboard_menu()
            dashboard_page.click_detection_list()
            dashboard_page.click_upload_btn()
            time.sleep(1)
            dashboard_page.input_file(const.PATH_FRECOG_IMG)
            time.sleep(1)
            dashboard_page.check_anothers_checkbox()

            return True, None
        except BaseException as exc:
            print("exc", exc)
            return False, exc