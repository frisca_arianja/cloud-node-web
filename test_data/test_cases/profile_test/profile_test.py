from test_data.src.pages.profile.profile_page import ProfilePage
from test_data.test_cases.test_data import const
from test_data.test_cases.utils.assert_element import AssertionElement
from skeleton.test_template import TestTemplate
from selenium.webdriver.common.by import By
import time

class MyProfile(TestTemplate):

    def __init__(self, driver):
        super().__init__(const.DOMAIN, driver)
