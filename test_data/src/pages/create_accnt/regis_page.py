import time
import json
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from skeleton.base_page import BasePage
from selenium.webdriver.common.by import By
from test_data.src.pages.login.login_page import LoginPage

class RegisPage(BasePage):
    create_new_account_text_xpath = '//*[@id="root"]/div/div[1]/div[1]/div[2]/a/span'
    title_regis_page_xpath = '//*[@id="root"]/div/div[1]/div[1]/div[1]'
    email_textfield_name = 'email'
    accnt_nm_textfield_name = 'username'
    password_textfield_name = 'password'
    confm_password_textfield_name = 'confirmPassword'
    title_success_page_xpath = '//*[@id="root"]/div/div[1]/div[1]/div[1]'
    sign_in_text_xpath = '//*[@id="root"]/div/div[1]/div[1]/div[2]/a/span'
    show_emsg_email_xpath = '//*[@id="root"]/div/div[1]/div[1]/form/div[1]/label[2]'
    show_emsg_accnt_nm_xpath = '//*[@id="root"]/div/div[1]/div[1]/form/div[2]/label[2]'
    show_emsg_password_xpath = '//*[@id="root"]/div/div[1]/div[1]/form/div[3]/label[2]'
    show_emsg_conf_password_xpath = '//*[@id="root"]/div/div[1]/div[1]/form/div[4]/label[2]'
    information_icon_xpath = '//*[@id="root"]/div/div[1]/div[1]/form/div[2]/label/img'
    tooltip_xpath = '//*[@id="usernameTooltip"]'
    password_icon_xpath = '//*[@id="root"]/div/div[1]/div[1]/form/div[3]/div/img'
    conf_password_icon_xpath = '//*[@id="root"]/div/div[1]/div[1]/form/div[4]/div/img'
    nodeflux_icon_xpath = '//*[@id="root"]/div/div[1]/a/img'
    msg_server_error_xpath = '/html/body/div[2]/div/div/div[1]'
    cancel_btn_xpath = '/html/body/div[2]/div/div/div[3]/button[1]'
    try_again_btn_xpath = '/html/body/div[2]/div/div/div[3]/button[2]'

    def setup_method(self, method):
        self.driver = webdriver.Chrome()
        self.vars = {}
    
    def teardown_method(self, method):
        self.driver.quit()

    def click_create_account(self):
        self._driver.find_element(
            By.XPATH, RegisPage.create_new_account_text_xpath).is_displayed()
        self._driver.find_element(
            By.XPATH, RegisPage.create_new_account_text_xpath).click()
    
    def title_regis_page(self): 
        title = self._driver.find_element(
            By.XPATH, RegisPage.title_regis_page_xpath).text
        return title

    def input_email(self, email: str):
        self._driver.find_element_by_name(
            RegisPage.email_textfield_name).is_displayed()
        self._driver.find_element_by_name(
            RegisPage.email_textfield_name).send_keys(email)

    def input_accnt_name(self, accnt_nm: str):
        self._driver.find_element_by_name(
            RegisPage.accnt_nm_textfield_name).is_displayed()
        self._driver.find_element_by_name(
            RegisPage.accnt_nm_textfield_name).clear()
        self._driver.find_element_by_name(
            RegisPage.accnt_nm_textfield_name).send_keys(accnt_nm)
    
    def input_password(self, password: str):
        self._driver.find_element_by_name(
            RegisPage.password_textfield_name).is_displayed()
        self._driver.find_element_by_name(
            RegisPage.password_textfield_name).send_keys(password)

    def input_confm_password(self, confm_password: str):
        self._driver.find_element_by_name(
            RegisPage.confm_password_textfield_name).is_displayed()
        self._driver.find_element_by_name(
            RegisPage.confm_password_textfield_name).send_keys(confm_password)

    def click_regis_button(self):
        self._driver.find_element(
            By.CSS_SELECTOR, "[type=default]").click()

    def check_server_error(self):
        msg_server_error = self._driver.find_element(
            By.XPATH, RegisPage.msg_server_error_xpath).text
        return msg_server_error

    def click_cancel_btn(self):
        self._driver.find_element(
            By.XPATH, RegisPage.cancel_btn_xpath).is_displayed()
        self._driver.find_element(
            By.XPATH, RegisPage.cancel_btn_xpath).click()

    def click_try_again_btn(self):
        self._driver.find_element(
            By.XPATH, RegisPage.try_again_btn_xpath).is_displayed()
        self._driver.find_element(
            By.XPATH, RegisPage.try_again_btn_xpath).click()

    def check_Go_to_Login_button(self):
        self._driver.find_element(
            By.CSS_SELECTOR, "[type=default]").is_displayed()
    
    def title_success_page(self): 
        title = self._driver.find_element(
            By.XPATH, RegisPage.title_success_page_xpath).text 
        return title

    def click_Go_to_Login_button(self):
        self._driver.find_element(
            By.CSS_SELECTOR, "[type=default]").click()

    def check_login_page(self): 
        wordingin_login_page = self._driver.find_element(
            By.XPATH, LoginPage.title_login_page_xpath).text
        return wordingin_login_page

    def click_sign_in_text(self):
        self._driver.find_element(
            By.XPATH, RegisPage.sign_in_text_xpath).is_displayed()
        self._driver.find_element(
            By.XPATH, RegisPage.sign_in_text_xpath).click()

    def check_email_error_msg(self): 
        error_msg = self._driver.find_element(
            By.XPATH, RegisPage.show_emsg_email_xpath).text 
        return error_msg

    def check_regis_btn_status(self):
        clicked_element = self._driver.find_element(
            By.CSS_SELECTOR, "[type=default]")
        if not clicked_element.is_enabled():
            print("Button is disabled")
        else:
            print("Button is enabled")
        return clicked_element

    def check_accnt_nm_error_msg(self): 
        error_msg = self._driver.find_element(
            By.XPATH, RegisPage.show_emsg_accnt_nm_xpath).text 
        return error_msg

    def check_password_error_msg(self): 
        error_msg = self._driver.find_element(
            By.XPATH, RegisPage.show_emsg_password_xpath).text 
        return error_msg

    def check_conf_password_error_msg(self): 
        error_msg = self._driver.find_element(
            By.XPATH, RegisPage.show_emsg_conf_password_xpath).text
        return error_msg

    def check_information_icon(self): 
        self._driver.find_element(
            By.XPATH, RegisPage.information_icon_xpath).is_displayed()
        self._driver.find_element(
            By.XPATH, RegisPage.information_icon_xpath).click()

    def check_tooltip_msg(self):
        tooltip_msg = self._driver.find_element(
            By.XPATH, RegisPage.tooltip_xpath).text
        return tooltip_msg

    def click_password_icon(self):
        self._driver.find_element(
            By.XPATH, RegisPage.password_icon_xpath).is_displayed()
        self._driver.find_element(
            By.XPATH, RegisPage.password_icon_xpath).click()
    
    def check_password_value(self):
        password_value = self._driver.find_element(
            By.NAME, RegisPage.password_textfield_name).get_attribute('value')
        return password_value
    
    def click_confm_password_icon(self):
        self._driver.find_element(
            By.XPATH, RegisPage.conf_password_icon_xpath).is_displayed()
        self._driver.find_element(
            By.XPATH, RegisPage.conf_password_icon_xpath).click()

    def check_confirm_password_value(self):
        confm_password_value = self._driver.find_element(
            By.NAME, RegisPage.confm_password_textfield_name).get_attribute('value')
        return confm_password_value

    def click_nodeflux_cloud_icon(self):
        self._driver.find_element(
            By.XPATH, RegisPage.nodeflux_icon_xpath).is_displayed()
        self._driver.find_element(
            By.XPATH, RegisPage.nodeflux_icon_xpath).click()