import time
import json
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from skeleton.base_page import BasePage

class ProfilePage(BasePage):

    profile_title_xpath = '//*[@id="__next"]/div/div[2]/div[3]/div[1]/span'
    usage_quota_title_xpath = '//*[@id="__next"]/div/div[2]/div[5]/div[2]/div[2]/div[1]'
    value_quota_free = '//*[@id="__next"]/div/div[2]/div[5]/div[2]/div[2]/div[3]/div[2]'
    quota_bar_xpath = '//*[@id="__next"]/div/div[2]/div[5]/div[2]/div[2]/div[4]'

    def usage_quota_title(self):
        self._driver.find_element(
            By.XPATH, ProfilePage.usage_quota_title_xpath).is_displayed()
        title = self._driver.find_element(
            By.XPATH, ProfilePage.usage_quota_title_xpath).text
        return title

    def check_quota_free(self):
        value_quota = self._driver.find_element(
            By.XPATH, ProfilePage.value_quota_free).text
        self._driver.find_element(
            By.XPATH, ProfilePage.quota_bar_xpath).is_displayed()        
        return value_quota
      