import time
import json
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from skeleton.base_page import BasePage
from selenium.webdriver.common.by import By

class LoginPage(BasePage):
    email_textfield_name = 'email'
    password_textfield_name = 'password'
    close_button_xpath = '/html/body/div[2]/div/div/div[3]/button'
    password_icon_xpath = '//*[@id="root"]/div/div[1]/div[1]/form/div[2]/div/img'
    emsg_activation_accnt = '/html/body/div[2]/div/div/div[2]'
    google_acnt_id = 'identifierId'
    next_button_xpath = '//*[@id="identifierNext"]/span/span'
    google_password_name = 'password'
    emsg_invalid_GAcnt = '//*[@id="view_container"]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div/div[2]/div[2]/div'
    nodeflux_icon_xpath = '//*[@id="root"]/div/div[1]/a/img'
    show_emsg_xpath = '//*[@id="root"]/div/div[1]/div[1]/form/div[2]/label[2]'
    title_login_page_xpath = '//*[@id="root"]/div/div[1]/div[1]/div[1]'
    
    vars = {}

    def input_email(self, email: str):
        self._driver.find_element_by_name(
            LoginPage.email_textfield_name).is_displayed()
        self._driver.find_element_by_name(
            LoginPage.email_textfield_name).send_keys(email)

    def input_password(self, password: str):
        self._driver.find_element_by_name(
            LoginPage.password_textfield_name).is_displayed()
        self._driver.find_element_by_name(
            LoginPage.password_textfield_name).send_keys(password)

    def click_login_button(self):
        self._driver.find_element(
            By.CSS_SELECTOR, "[type=default]").click()

    def click_close_button(self):
        self._driver.find_element(
            By.XPATH, LoginPage.close_button_xpath).is_displayed()
        self._driver.find_element(
            By.XPATH, LoginPage.close_button_xpath).click()
    
    def check_error_msg(self): 
        error_msg = self._driver.find_element(
            By.XPATH, LoginPage.show_emsg_xpath).text 
        return error_msg

    def click_password_icon(self):
        self._driver.find_element(
            By.XPATH, LoginPage.password_icon_xpath).is_displayed()
        self._driver.find_element(
            By.XPATH, LoginPage.password_icon_xpath).click()
    
    def check_password_value(self):
        password_value = self._driver.find_element(
            By.NAME, LoginPage.password_textfield_name).get_attribute('value')
        return password_value

    def click_sign_in_google_accnt(self):
        self.vars["window_handles"] = self._driver.window_handles
        self._driver.find_element(
            By.CSS_SELECTOR, "[type=google]").click()

    def wait_for_window(self, timeout = 2):
        time.sleep(round(timeout / 1000))
        wh_now = self._driver.window_handles
        wh_then = self.vars["window_handles"]
        if len(wh_now) > len(wh_then):
            return set(wh_now).difference(set(wh_then)).pop()

    def input_google_accnt(self, google_email: str):
        self.vars["popup_window"] = self.wait_for_window(2000)
        self._driver.switch_to.window(self.vars["popup_window"])
        self._driver.find_element_by_id(
            LoginPage.google_acnt_id).is_displayed()
        self._driver.find_element_by_id(
            LoginPage.google_acnt_id).send_keys(google_email)

    def click_next_button(self):
        self._driver.find_element(
            By.XPATH, LoginPage.next_button_xpath).click()
    
    def input_password_google_acnt(self,google_password: str):
        self._driver.find_element(By.NAME, "password").click()
        self._driver.find_element(By.NAME, "password").send_keys(google_password)
        self._driver.find_element(By.CSS_SELECTOR, "#passwordNext > .CwaK9").click()

    def error_invalid_google_accnt(self):
        self.vars["popup_window"] = self.wait_for_window(2000)
        self._driver.switch_to.window(self.vars["popup_window"])
        emsg_invalid_gacnt = self._driver.find_element(
            By.XPATH, LoginPage.emsg_invalid_GAcnt).text 
        print(emsg_invalid_gacnt)
        return emsg_invalid_gacnt

    def check_activation_emsg(self): 
        error_msg_activation = self._driver.find_element(
            By.XPATH, LoginPage.emsg_activation_accnt).text 
        return error_msg_activation
    
    def click_nodeflux_cloud_icon(self):
        self._driver.find_element(
            By.XPATH, LoginPage.nodeflux_icon_xpath).is_displayed()
        self._driver.find_element(
            By.XPATH, LoginPage.nodeflux_icon_xpath).click()

    def check_login_page(self): 
        title_login_page = self._driver.find_element(
            By.XPATH, LoginPage.title_login_page_xpath).text
        return title_login_page