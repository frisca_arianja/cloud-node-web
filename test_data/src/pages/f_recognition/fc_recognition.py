import time
import json
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from skeleton.base_page import BasePage

class FaceRecognition_Page(BasePage):
    
    fr_text_xpath = '//div[@class="ReactModalPortal"][3]/div/div/div/div[2]/div[3]/label[1]/div'
    fr_checkbox_xpath = '//div[@class="ReactModalPortal"][3]/div/div/div/div[2]/div[3]/label[1]/div/label/span'
    first_result_xpath = '//*[@id="__next"]/div/div[2]/div[7]/div[1]/div[2]/div/div/div[2]/div[1]'
    second_result_xpath = '//*[@id="__next"]/div/div[2]/div[7]/div[1]/div[2]/div/div/div[2]/div[2]'

    fr_tab_xpath = '//*[@id="1"]/span'
    fr_tab2_xpath = '//*[@id="2"]/span'
    #sometimes because fr tab not in constant position every multiple hit to analytic 

    fr_icon_tab_xpath = '//*[@id="1"]/svg'

    fr_section_text_xpath = '//*[@id="0"]/div/div/div[2]/div[2]/div/div[3]/div/div/div[1]/p'
    identified_section_text_xpath = '//*[@id="0"]/div/div/div[2]/div[2]/div/div[3]/div/div/div[2]/p'
    unknown_person_text_xpath = '//*[@id="0"]/div/div/div[2]/div[2]/div/div[3]/div/div/div[3]/p'

    recog_face_crops_xpath = '//*[@id="0"]/div/div/div[2]/div/div/div[3]/div/div/div[2]/div/div/div/div/div[1]'
    recog_name_person_xpath = '//*[@id="0"]/div/div/div[2]/div/div/div[3]/div/div/div[2]/div/div/div/div/div[2]'
    face_id_unrecog_xpath = '//*[@id="0"]/div/div/div[2]/div/div/div[3]/div/div/div[3]/div/div/div/div[2]/div[1]'
    percentage_of_similarity_xpath = '//*[@id="0"]/div/div/div[2]/div/div/div[3]/div/div/div[2]/div/div/div/div/div[3]'

    unrecog_face_crops_xpath = '//*[@id="0"]/div/div/div[2]/div/div/div[3]/div/div/div[3]/div/div/div/div[1]'
    unrecog_text_xpath = '//*[@id="0"]/div/div/div[2]/div/div/div[3]/div/div/div[3]/div/div/div/div[3]'
    close_list_xpath = '//*[@id="0"]/svg'
    
    hover_face_crops = '//*[@id="0"]/div/div/div[2]/div/div/div[3]/div/div/div[2]/div/div/div/div'
    show_detail_btn_xpath = '//*[@id="0"]/div/div/div[2]/div/div/div[3]/div/div/div[2]/div/div/div/div/button'

    total_person_text_xpath = '//*[@id="0"]/div/div/div[2]/div/div/div[3]/div/div/div[1]/div/div[1]/span[1]'
    total_person_xpath = '//*[@id="0"]/div/div/div[2]/div/div/div[3]/div/div/div[1]/div/div[1]/span[2]'

    identified_person_text_xpath = '//*[@id="0"]/div/div/div[2]/div/div/div[3]/div/div/div[1]/div/div[2]/span[1]'
    identified_person_xpath = '//*[@id="0"]/div/div/div[2]/div/div/div[3]/div/div/div[1]/div/div[2]/span[2]'

    fr_detail_popup_xpath = '//div[contains(@class, "ReactModalPortal")][4]/div/div/div/div[1]/h4'
    go_to_enrollment_xpath = '/html/body/div[8]/div/div/div/div[1]/div/div[3]/div[2]/div/div[1]/a'

    def fr_checkbox_text(self):
        text = self._driver.find_element(
            By.XPATH, FaceRecognition_Page.fr_text_xpath).text
        return text

    def click_fr_checkbox(self):
        self._driver.find_element(
            By.XPATH, FaceRecognition_Page.fr_checkbox_xpath).click()

    def check_result(self):
        self._driver.find_element(
            By.XPATH, FaceRecognition_Page.first_result_xpath).click()

    def more_than_one_result(self):
        one = self._driver.find_element(
            By.XPATH, FaceRecognition_Page.first_result_xpath)
        two = self._driver.find_element(
            By.XPATH, FaceRecognition_Page.second_result_xpath)
        if one and two.is_displayed():
            print("success insert 2 list in detection list")
        else : 
            print("not insert 2 list in detection list")

    def check_fr_tab(self):
        self._driver.find_element(
            By.XPATH, FaceRecognition_Page.fr_tab_xpath).is_displayed()
        text = self._driver.find_element(
            By.XPATH, FaceRecognition_Page.fr_tab_xpath).text
        print(text)
        return text    

    def click_fr_tab(self):
        self._driver.find_element(
            By.XPATH, FaceRecognition_Page.fr_tab2_xpath).is_displayed()
        text = self._driver.find_element(
            By.XPATH, FaceRecognition_Page.fr_tab2_xpath).text
        print(text)
        self._driver.find_element(
            By.XPATH, FaceRecognition_Page.fr_tab2_xpath).click()
        return text

    def section1(self):
        text1 = self._driver.find_element(
            By.XPATH, FaceRecognition_Page.fr_section_text_xpath).text
        return text1

    def section2(self):
        text2 = self._driver.find_element(
            By.XPATH, FaceRecognition_Page.identified_section_text_xpath).text
        return text2

    def section3(self):
        text3 = self._driver.find_element(
            By.XPATH, FaceRecognition_Page.unknown_person_text_xpath).text
        return text3

    def recognized_detail(self):
        self._driver.find_element(
            By.XPATH, FaceRecognition_Page.recog_face_crops_xpath).is_displayed()
        text1 = self._driver.find_element(
            By.XPATH, FaceRecognition_Page.recog_name_person_xpath).text
        print(text1)
        text2 = self._driver.find_element(
            By.XPATH, FaceRecognition_Page.percentage_of_similarity_xpath).text
        print(text2)
        return text1, text2

    def unrecognized_detail(self):
        self._driver.find_element(
            By.XPATH, FaceRecognition_Page.unrecog_face_crops_xpath).is_displayed()
        text1 = self._driver.find_element(
            By.XPATH, FaceRecognition_Page.face_id_unrecog_xpath).text
        print(text1)
        text2 = self._driver.find_element(
            By.XPATH, FaceRecognition_Page.unrecog_text_xpath).text
        print(text2)
        return text1, text2

    def click_close_list(self):
        self._driver.find_element(
            By.XPATH, FaceRecognition_Page.close_list_xpath).click()

    def result_no_face(self):
        title1 = self._driver.find_element(
            By.XPATH, FaceRecognition_Page.total_person_text_xpath).text
        number = self._driver.find_element(
            By.XPATH, FaceRecognition_Page.total_person_xpath).text
        title2 = self._driver.find_element(
            By.XPATH, FaceRecognition_Page.identified_person_text_xpath).text
        number_identified = self._driver.find_element(
            By.XPATH, FaceRecognition_Page.identified_person_xpath).text
        return title1, number, title2, number_identified
 
    def click_show_detail(self):
        self._driver.find_element(
            By.XPATH, FaceRecognition_Page.hover_face_crops).click()
        self._driver.find_element(
            By.XPATH, FaceRecognition_Page.show_detail_btn_xpath).click()
        time.sleep(2)

    def title_detail_fr(self):
        text1 = self._driver.find_element(
            By.XPATH, FaceRecognition_Page.fr_detail_popup_xpath).text
        print(text1)
        return text1

    def go_to_enrollment(self):
        text = self._driver.find_element(
            By.XPATH, FaceRecognition_Page.go_to_enrollment_xpath).text
        self._driver.find_element(
            By.XPATH, FaceRecognition_Page.go_to_enrollment).click()
        return text

    def close_detail(self):
        self._driver.find_elements(
            By.CSS_SELECTOR, "[type=default]")[0].click()
    
    def fr_icon_tab(self):
        self._driver.find_element(
            By.XPATH, FaceRecognition_Page.fr_icon_tab_xpath).is_displayed()