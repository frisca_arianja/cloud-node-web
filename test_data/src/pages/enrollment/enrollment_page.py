import time
import json
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from skeleton.base_page import BasePage

class EnrollmentPage(BasePage):

    title_enrollment_popup_xpath = '/html/body/div[8]/div/div/div/div[1]/div[1]'

    def title_enrollment_xpath(self):
        title = self._driver.find_element(
            By.XPATH, EnrollmentPage.title_enrollment_popup_xpath).text
        return title