import time
import json
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from skeleton.base_page import BasePage

class BillingPage(BasePage):

    billing_sidebar_name = 'billing'
    billing_title_xpath = '//*[@id="__next"]/div/div[2]/div[3]/div[1]'
    tooltip_xpath = '//div[@id="warning"]'
    warning_icon_id = 'warning'

    def check_billing_menu(self):
        self._driver.find_element(
            By.NAME, BillingPage.billing_sidebar_name).is_displayed()
        billing_menu = self._driver.find_element(
            By.NAME, BillingPage.billing_sidebar_name).text
        return billing_menu

    def click_billing_menu(self):
        self._driver.find_element(
            By.NAME, BillingPage.billing_sidebar_name).click()

    def check_billing_title(self):
        self._driver.find_element(
            By.XPATH, BillingPage.billing_title_xpath).is_displayed()
        billing_title_page = self._driver.find_element(
            By.XPATH, BillingPage.billing_title_xpath).text
        return billing_title_page

    def check_warning_icon(self):
        self._driver.find_element(
             By.ID, BillingPage.warning_icon_id).is_displayed()
        self._driver.find_element(
            By.ID, BillingPage.warning_icon_id).click()

        #notes : search how to hover multiple element and check the value,
        #notes : how check if warning icon more than one 

    def check_tooltip_msg(self):
        tooltip_msg = self._driver.find_element(
            By.XPATH, BillingPage.tooltip_xpath).text
        return tooltip_msg  