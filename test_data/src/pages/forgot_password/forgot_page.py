import time
import json
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from skeleton.base_page import BasePage
from selenium.webdriver.common.by import By


class ForgotPasswordPage(BasePage):
    forgot_password_text_xpath = '//*[@id="root"]/div/div[1]/div[1]/form/div[3]/a/span'
    title_forgot_password_xpath = '//*[@id="root"]/div/div[1]/div[1]/div[1]'
    email_textfield_name = 'email'
    checkbox_recaptcha_id = 'recaptcha-anchor'
    emsg_show = '//*[@id="root"]/div/div[1]/div[1]/div[3]/label[2]'

    def setup_method(self, method):
        self.driver = webdriver.Chrome()
        self.vars = {}
    
    def teardown_method(self, method):
        self.driver.quit()
    
    def click_forgot_password(self):
        self._driver.find_element(
            By.XPATH, ForgotPasswordPage.forgot_password_text_xpath).is_displayed()
        self._driver.find_element(
            By.XPATH, ForgotPasswordPage.forgot_password_text_xpath).click()
    
    def title_page(self): 
        title = self._driver.find_element(
            By.XPATH, ForgotPasswordPage.title_forgot_password_xpath).text 
        return title

    def check_submit_button_status(self):
        submit_button_status = self._driver.find_element(
            By.CSS_SELECTOR, "[type=default]")

        if not submit_button_status.is_enabled():
            print ("Button is disabled")
        else:
            submit_button_status.click()

    def input_email(self, email: str):
        self._driver.find_element_by_name(
            ForgotPasswordPage.email_textfield_name).is_displayed()
        self._driver.find_element_by_name(
            ForgotPasswordPage.email_textfield_name).send_keys(email)

    def click_checkbox_recaptcha(self):
        self._driver.find_element(
            By.ID, ForgotPasswordPage.checkbox_recaptcha_id).is_displayed()
        self._driver.find_element(
            By.ID, ForgotPasswordPage.checkbox_recaptcha_id).click()
  
    def check_error_msg(self): 
        error_msg = self._driver.find_element(
            By.XPATH, ForgotPasswordPage.emsg_show).text 
        return error_msg