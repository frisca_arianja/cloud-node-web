import time
import json
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from skeleton.base_page import BasePage

class DashboardPage(BasePage):

    dashboard_sidebar_name = 'dashboard'
    dashboard_title_xpath = '//*[@id="__next"]/div/div[2]/div[4]/div[1]'
    activated_accnt_msg_xpath = '//*[@id="__next"]/div/div[2]/div[4]/span'
    click_here_text_xpath = '//*[@id="__next"]/div/div[2]/div[4]/div'
    detection_list_tab_xpath = '//*[@id="__next"]/div/div[2]/div[5]/div/a'
    quota_islow_xpath = '//*[@id="__next"]/div/div[2]/div[2]/span'
    click_check_billing_xpath = '//*[@id="__next"]/div/div[2]/div[2]/a'

    input_quick_search_xpath = '//*[@id="__next"]/div/div[2]/div[7]/div[1]/div[1]/div/div[1]/div/input'
    status_xpath = '//*[@id="__next"]/div/div[2]/div[7]/div[1]/div[2]/div/div/div[2]/div[1]/div[1]/div[3]/p'
    
    sort_by_button_xpath = '//*[@id="__next"]/div/div[2]/div[7]/div[1]/div[1]/div/div[1]/div/span'
    sort_by_analytic_xpath = '//*[@id="__next"]/div/div[2]/div[7]/div[1]/div[1]/div/div[1]/div/div[1]/div[1]'
    click_arrow_xpath= '//*[@id="__next"]/div/div[2]/div[7]/div[1]/div[1]/div/div[1]/span'
    sort_by_date_xpath = ' //*[@id="__next"]/div/div[2]/div[7]/div[1]/div[1]/div/div[1]/div/div[1]/div[2]'
    value_date_xpath = '//*[@id="__next"]/div/div[2]/div[7]/div[1]/div[2]/div/div/div[2]/div[1]/div[1]/div[1]/p'
    sort_by_status_xpath = '//*[@id="__next"]/div/div[2]/div[7]/div[1]/div[1]/div/div[1]/div/div[1]/div[3]'
    
    filter_button_id = 'data-list-filter-button'
    click_date_from_xpath = '//div[@class="ReactModalPortal"][1]/div/div/div[1]/div[1]/label/div/div/div/div/input'
    choose_date_from_xpath = '//div[@class="ReactModalPortal"][1]/div/div/div[1]/div[1]/label/div[2]/div/div[2]/div[2]/div[5]/div[2]'
    result_f_onedt_xpath = '//*[@id="__next"]/div/div[2]/div[7]/div[1]/div[2]/div/div/div[2]/div[1]/div[1]/div[1]/p'

    click_date_to_xpath = '//div[@class="ReactModalPortal"][1]/div/div/div[1]/div[1]/label[2]/div/div/div/div/input'
    choose_date_to_xpath = '//div[@class="ReactModalPortal"][1]/div/div/div[1]/div[1]/label[2]/div[2]/div/div[2]/div[2]/div[2]/div[2]'
    result_f_rangedt_xpath = '//*[@id="__next"]/div/div[2]/div[7]/div[1]/div[2]/div/div/div[2]/div[1]/div[1]/div[1]/p'
    next_result_xpath = '//*[@id="__next"]/div/div[2]/div[7]/div[2]/div[2]/div[3]/a[2]'

    close_cal_xpath = '//div[@class="ReactModalPortal"][1]/div/div'
    clear_all_filter_xpath = '//div[@class="ReactModalPortal"][1]/div/div/div[2]'
  
    filter_fdemo_xpath = '//div[@class="ReactModalPortal"][1]/div/div/div[1]/div[2]/div[3]/label[1]'
    # //div[@class="ReactModalPortal"][1]/div/div/div[1]/div[2]/div[3]/label[4]'
    filter_frecog_xpath = '//div[@class="ReactModalPortal"][1]/div/div/div[1]/div[2]/div[3]/label[2]'
    # /div/div/div[1]/div[2]/div[3]/label[1]'
    filter_status_xpath = '//div[@class="ReactModalPortal"][1]/div/div/div[1]/div[3]/div[3]/label[2]'
    
    scroll_xpath = '//div[@class="ReactModalPortal"][1]/div/div/div[1]/div[4]/div[3]'
    filter_access_key_xpath = '//div[@class="ReactModalPortal"][1]/div/div/div[1]/div[4]/div[3]/label[3]'
    result_access_key_xpath = '//*[@id="__next"]/div/div[2]/div[7]/div[1]/div[2]/div/div/div[2]/div[1]/div[1]/div[2]'

    upload_btn_name = 'upload'
    upload_popup_title_xpath = '//div[@class="ReactModalPortal"][2]/div/div/div[2]/div[1]/p'
    close_browse_popup_xpath = '//div[@class="ReactModalPortal"][2]/div/div/img'

    close_upload_popup_xpath = '//div[@class="ReactModalPortal"][3]/div/div/img'
    remove_img_xpath = '//div[@class="ReactModalPortal"][3]/div/div/div/div[1]/div[3]/div/div[2]/img'
    cancel_upload_xpath = '//div[@class="ReactModalPortal"][3]/div/div/div/div[3]/div[2]/button[1]'
    done_btn_name = 'done'

    file_uploaded = '//div[@class="ReactModalPortal"][3]/div/div/div/div[1]/div[3]/div/div[2]/div/img'
    multiple_uploaded = '//div[@class="ReactModalPortal"][3]/div/div/div/div[1]/div[3]/div/div[3]/div/img'

    valid_file_uploaded = '//div[@class="ReactModalPortal"][3]/div/div/div/div[1]/div[4]/div/div[2]/div/img'
    pagination_number_xpath = '//*[@id="__next"]/div/div[2]/div[7]/div[2]/div[2]/div[3]/a[3]'

    fd_text_xpath = '//div[@class="ReactModalPortal"][3]/div/div/div/div[2]/div[3]/label[1]/div'
    fd_checkbox_xpath = '//div[@class="ReactModalPortal"][3]/div/div/div/div[2]/div[3]/label[1]/div/label/span'

    fm_text_xpath = '//div[@class="ReactModalPortal"][3]/div/div/div/div[2]/div[3]/label[4]/div'
    fm_checkbox_xpath = '//div[@class="ReactModalPortal"][3]/div/div/div/div[2]/div[3]/label[2]/div/label/span'
    fr_checkbox_xpath = '//div[@class="ReactModalPortal"][3]/div/div/div/div[2]/div[3]/label[2]/div/label/span'
    planogram_text_xpath = '//div[@class="ReactModalPortal"][3]/div/div/div/div[2]/div[3]/label[3]/div'
    planogram_xpath = '//div[@class="ReactModalPortal"][3]/div/div/div/div[2]/div[3]/label[3]/div/label/span'

    lpr_text_xpath = '//div[@class="ReactModalPortal"][3]/div/div/div/div[2]/div[3]/label[3]/div'
    lpr_checkbox_xpath = '//div[@class="ReactModalPortal"][3]/div/div/div/div[2]/div[3]/label[3]/div/label/span'

    error_validation_img = '//div[@class="ReactModalPortal"][3]/div/div/div/div[1]/div[3]'
    error_max_size = '//div[@class="ReactModalPortal"][2]/div/div/div[2]/div[1]/div' 

    add_photo_xpath = '//div[@class="ReactModalPortal"][3]/div/div/div/div[1]/div[3]/div/div[1]/div'
    warning_icon_xpath = '//div[@class="ReactModalPortal"][3]/div/div/div/div[2]/div[3]/label[3]/div/div/svg'
    warning_on_checkbox = 'select'
    # '//div[@class="ReactModalPortal"][3]/div/div/div/div[2]/div[3]/label[4]/div/div/svg'
    tooltip_checkbox_xpath = '//*[@id="select"]'

    def check_dashboard_menu(self):
        self._driver.find_element(
            By.NAME, DashboardPage.dashboard_sidebar_name).is_displayed()
        dashboard_menu = self._driver.find_element(
            By.NAME, DashboardPage.dashboard_sidebar_name).text
        return dashboard_menu

    def click_dashboard_menu(self):
        self._driver.find_element(
            By.NAME, DashboardPage.dashboard_sidebar_name).click()
        dashboard_title_page = self._driver.find_element(
            By.XPATH, DashboardPage.dashboard_title_xpath).text
        return dashboard_title_page

    def msg_activated(self):
        info_msg = self._driver.find_element(
            By.XPATH, DashboardPage.activated_accnt_msg_xpath).text
        return info_msg
    
    def check_click_here_text(self):
        text = self._driver.find_element(
            By.XPATH, DashboardPage.click_here_text_xpath).text
        return text

    def click_here(self):
        self._driver.find_element(
            By.XPATH, DashboardPage.click_here_text_xpath).click()
    
    def quota_msg(self):
        info_msg = self._driver.find_element(
            By.XPATH, DashboardPage.quota_islow_xpath).text
        return info_msg

    def check_billing_text(self):
        text = self._driver.find_element(
            By.XPATH, DashboardPage.click_check_billing_xpath).text
        return text

    def click_check_billing(self):
        self._driver.find_element(
            By.XPATH, DashboardPage.click_check_billing_xpath).click()

    def click_detection_list(self):
        self._driver.find_element(
            By.XPATH, DashboardPage.detection_list_tab_xpath).click()
        detection_title_page = self._driver.find_element(
            By.XPATH, DashboardPage.detection_list_tab_xpath).text
        return detection_title_page

    def input_quick_search(self, search: str):
        self._driver.find_element(
            By.CSS_SELECTOR, "[type=text]").is_displayed()
        self._driver.find_element(
            By.CSS_SELECTOR, "[type=text]").send_keys(search)

    def find_status_processing(self):
        status_processing = self._driver.find_element(
            By.XPATH, DashboardPage.status_xpath).text
        return status_processing

    def click_sort_by_button(self):
        self._driver.find_element(
            By.XPATH, DashboardPage.sort_by_button_xpath).is_displayed()
        sortby_text = self._driver.find_element(
            By.XPATH, DashboardPage.sort_by_button_xpath).text
        self._driver.find_element(
            By.XPATH, DashboardPage.sort_by_button_xpath).click()
        return sortby_text

    def choose_sortby_analytic(self):
        self._driver.find_element(
            By.XPATH, DashboardPage.sort_by_analytic_xpath).is_displayed()
        sortby_analytic = self._driver.find_element(
            By.XPATH, DashboardPage.sort_by_analytic_xpath).text
        self._driver.find_element(
            By.XPATH, DashboardPage.sort_by_analytic_xpath).click()
        return sortby_analytic
    
    def click_arrow_symbol(self):
        self._driver.find_element(
            By.XPATH, DashboardPage.click_arrow_xpath).click()

    def choose_sortby_date(self):
        self._driver.find_element(
            By.XPATH, DashboardPage.sort_by_date_xpath).is_displayed()
        sortby_date = self._driver.find_element(
            By.XPATH, DashboardPage.sort_by_date_xpath).text
        self._driver.find_element(
            By.XPATH, DashboardPage.sort_by_date_xpath).click()
        return sortby_date

    def check_date_sorting(self): 
        date_value = self._driver.find_element(
            By.XPATH, DashboardPage.value_date_xpath).text
        return date_value

    def choose_sortby_status(self):
        self._driver.find_element(
            By.XPATH, DashboardPage.sort_by_status_xpath).is_displayed()
        sortby_status = self._driver.find_element(
            By.XPATH, DashboardPage.sort_by_status_xpath).text
        self._driver.find_element(
            By.XPATH, DashboardPage.sort_by_status_xpath).click()
        return sortby_status

    def check_status_sorting(self): 
        status_value = self._driver.find_element(
            By.XPATH, DashboardPage.status_xpath).text
        return status_value

    def check_filter_text(self):
        self._driver.find_element(
            By.ID, DashboardPage.filter_button_id).is_displayed()
        filter_text = self._driver.find_element(
            By.ID, DashboardPage.filter_button_id).text
        self._driver.find_element(
            By.ID, DashboardPage.filter_button_id).click()
        return filter_text

    def choose_one_time(self): 
        self._driver.find_element(
            By.CSS_SELECTOR, "[value=one]").click()

    def click_date_from(self): 
        self._driver.find_element(
            By.XPATH, DashboardPage.click_date_from_xpath).click()

    def click_prev_month(self):
        self._driver.find_element(
            By.CSS_SELECTOR, "[type=button]").click()
    
    def choose_date_from(self):
        self._driver.find_element(
            By.XPATH, DashboardPage.choose_date_from_xpath).click()

    def close_calender(self): 
        self._driver.find_element(
            By.XPATH, DashboardPage.close_cal_xpath).click()

    def close_filter_popup(self): 
        webdriver.ActionChains(self._driver).send_keys(Keys.ESCAPE).perform()

    def check_result_onedt(self):
        onedt_val = self._driver.find_element(
            By.XPATH, DashboardPage.result_f_onedt_xpath).text
        return onedt_val

    def choose_range_time(self): 
        self._driver.find_element(
            By.CSS_SELECTOR, "[value=two]").click()

    def click_date_to(self): 
        self._driver.find_element(
            By.XPATH, DashboardPage.click_date_to_xpath).click()

    def click_next_month(self):
        self._driver.find_elements(
            By.CSS_SELECTOR, "[type=button]")[1].click()

    def choose_date_to(self):
        self._driver.find_element(
            By.XPATH, DashboardPage.choose_date_to_xpath).click()  

    def check_result_rangedt(self):
        result = self._driver.find_element(
            By.XPATH, DashboardPage.result_f_rangedt_xpath).text
        return result

    def next_result(self):
        self._driver.find_element(
            By.XPATH, DashboardPage.next_result_xpath).click()

    def choose_face_demo(self): 
        title = self._driver.find_element(
            By.XPATH, DashboardPage.filter_fdemo_xpath).text
        self._driver.find_element(
            By.XPATH, DashboardPage.filter_fdemo_xpath).click()
        return title

    def choose_face_recog(self): 
        title = self._driver.find_element(
            By.XPATH, DashboardPage.filter_frecog_xpath).text
        self._driver.find_element(
            By.XPATH, DashboardPage.filter_frecog_xpath).click()
        return title

    def choose_processing_status(self): 
        title = self._driver.find_element(
            By.XPATH, DashboardPage.filter_status_xpath).text
        self._driver.find_element(
            By.XPATH, DashboardPage.filter_status_xpath).click()
        return title

    def choose_access_key(self): 
        search_ak = self._driver.find_element(
                By.XPATH, DashboardPage.filter_access_key_xpath)
        self._driver.execute_script("arguments[0].scrollIntoView();",search_ak)
        time.sleep(3)
        access_key = search_ak.text
        search_ak.click()
        time.sleep(3)
        return access_key       

    def check_result_access_key(self):
        result = self._driver.find_element(
            By.XPATH, DashboardPage.result_access_key_xpath).text
        return result

    def clear_all_filter(self): 
        self._driver.find_element(
            By.XPATH, DashboardPage.clear_all_filter_xpath).click()

    def check_analytic_checkbox_status(self):
        checkbox_fd = self._driver.find_element(
            By.XPATH, DashboardPage.filter_fdemo_xpath)
        checkbox_fr = self._driver.find_element(
            By.XPATH, DashboardPage.filter_frecog_xpath)
        if not checkbox_fd and checkbox_fr.is_enabled():
            print("Checkbox is enabled")
        else:
            print("Checkbox is disable")
        return checkbox_fd and checkbox_fr

    def check_upload_btn (self):
        self._driver.find_element(
            By.NAME, DashboardPage.upload_btn_name).is_displayed()
        upload_btn_text = self._driver.find_element(
            By.NAME, DashboardPage.upload_btn_name).text
        return upload_btn_text

    def click_upload_btn (self):
        self._driver.find_element(
            By.NAME, DashboardPage.upload_btn_name).click()

    def upload_popup_title(self):
        title = self._driver.find_element(
            By.XPATH, DashboardPage.upload_popup_title_xpath).text
        return title

    def close_browse_popup(self):
        self._driver.find_element(
            By.XPATH, DashboardPage.close_browse_popup_xpath).click()

    def close_upload_popup(self):
        self._driver.find_element(
            By.XPATH, DashboardPage.close_upload_popup_xpath).click()

    def input_file(self, file_loc: str) -> None:
        self._driver.find_element(
            By.CSS_SELECTOR, "[type=file]").send_keys(file_loc)
    
    def click_remove_img(self):
        self._driver.find_element(
            By.XPATH, DashboardPage.remove_img_xpath).click()

    def  click_cancel_btn(self):
        self._driver.find_element(
            By.XPATH, DashboardPage.cancel_upload_xpath).click()

    def  face_demo_checkbox(self):
        fd_text = self._driver.find_element(
            By.XPATH, DashboardPage.fd_text_xpath).text
        self._driver.find_element(
            By.XPATH, DashboardPage.fd_checkbox_xpath).click()
        return fd_text

    def check_done_btn_status(self):
        done_btn = self._driver.find_element(
            By.NAME, DashboardPage.done_btn_name)
        if done_btn.is_enabled():
            print("Done button is enabled")
        else:
            print("Done button is disable")
        return done_btn

    def check_file_uploaded(self): 
        uploaded = self._driver.find_element(
            By.XPATH, DashboardPage.file_uploaded)
        if uploaded.is_displayed():
            print("file uploaded")
        else:
            print("file not uploaded")
        return uploaded

    def check_multifile_uploaded(self): 
        first_upload = self._driver.find_element(
            By.XPATH, DashboardPage.file_uploaded)
        second_upload = self._driver.find_element(
            By.XPATH, DashboardPage.multiple_uploaded)
        if first_upload and second_upload.is_displayed():
            print("more than one file uploaded")
        else:
            print("not upload more than one file")
        return first_upload and second_upload

    def check_valid_file(self): 
        uploaded = self._driver.find_element(
            By.XPATH, DashboardPage.valid_file_uploaded)
        if uploaded.is_displayed():
            print("valid file uploaded")
        else:
            print("valid file not uploaded")
        return uploaded

    def check_error_validation (self):
        error_msg = self._driver.find_element(
            By.XPATH, DashboardPage.error_validation_img).text
        return error_msg

    def check_error_max_size (self):
        error_msg = self._driver.find_element(
            By.XPATH, DashboardPage.error_max_size).text
        return error_msg

    def check_add_photo (self):
        title = self._driver.find_element(
            By.XPATH, DashboardPage.add_photo_xpath).text
        self._driver.find_element(
            By.XPATH, DashboardPage.add_photo_xpath).click()
        return title
    
    def check_add_photo_status1(self):
        add_photo_btn = self._driver.find_element(
            By.XPATH, DashboardPage.add_photo_xpath)
        if not add_photo_btn.is_enabled():
            print("Add photo button is enabled")
        else:
            print("Add photo button is disable")
        return add_photo_btn

    def check_add_photo_status2(self):
        add_photo_btn = self._driver.find_element(
            By.XPATH, DashboardPage.add_photo_xpath)
        if add_photo_btn.is_enabled():
            print("Add photo button is enabled")
        else:
            print("Add photo button is disable")
        return add_photo_btn

    def click_done_btn(self):
        self._driver.find_element(
            By.NAME, DashboardPage.done_btn_name).click()

    def check_face_match_checkbox(self):
        fm_checkbox = self._driver.find_element(
            By.XPATH, DashboardPage.fm_checkbox_xpath)
        if not fm_checkbox.is_enabled():
            print("Face Match Checkbox is enabled")
        else:
            print("Face Match Checkbox is disable")

    def face_match_checkbox(self):
        fm_text = self._driver.find_element(
            By.XPATH, DashboardPage.fm_text_xpath).text
        self._driver.find_element(
            By.XPATH, DashboardPage.fm_checkbox_xpath).click()
        return fm_text

    def planogram_checkbox(self):
        planogram_text = self._driver.find_element(
            By.XPATH, DashboardPage.planogram_text_xpath).text
        self._driver.find_element(
            By.XPATH, DashboardPage.planogram_xpath).click()
        return planogram_text
    
    def check_planogram_checkbox(self):
        planogram_checkbox = self._driver.find_element(
            By.XPATH, DashboardPage.planogram_xpath)
        if not planogram_checkbox.is_enabled():
            print("Planogram Checkbox is enabled")
        else:
            print("Planogram Checkbox is disable")

    def check_anothers_checkbox(self):
        checkbox_fd = self._driver.find_element(
            By.XPATH, DashboardPage.fd_checkbox_xpath).click()
        checkbox_fr = self._driver.find_element(
            By.XPATH, DashboardPage.fr_checkbox_xpath).click()
        if  checkbox_fd and checkbox_fr.is_enabled():
            print("Analytics Checkbox is enabled")
        else:
            print("Analytics Checkbox is disable")
        return checkbox_fd and checkbox_fr

    def  lpr_checkbox1(self):
        lpr_text = self._driver.find_element(
            By.XPATH, DashboardPage.lpr_text_xpath).text
        lpr_checkbox = self._driver.find_element(
            By.XPATH, DashboardPage.lpr_checkbox_xpath)
        if not lpr_checkbox.is_enabled():
            print("LPR Checkbox is enabled")
        else:
            print("LPR Checkbox is disable")
        return lpr_text

    def  lpr_checkbox2(self):
        lpr_text = self._driver.find_element(
            By.XPATH, DashboardPage.lpr_text_xpath).text
        lpr_checkbox = self._driver.find_element(
            By.XPATH, DashboardPage.lpr_checkbox_xpath)
        if lpr_checkbox.is_enabled():
            print("LPR Checkbox is enabled")
        else:
            print("LPR Checkbox is disable")
        return lpr_text

    def click_lpr_checkbox (self):
        self._driver.find_element(
            By.XPATH, DashboardPage.lpr_checkbox_xpath).click()

    def check_warning_icon(self):
        self._driver.find_element(
             By.XPATH, DashboardPage.warning_icon_xpath).is_displayed()
        self._driver.find_element(
            By.XPATH, DashboardPage.warning_icon_xpath).click()

    def check_warning_checkbox(self):
        self._driver.find_element(
            By.ID, DashboardPage.warning_on_checkbox).is_displayed()
        self._driver.find_element(
            By.ID, DashboardPage.warning_on_checkbox).click()
        
    def check_tooltip_checkbox(self):
        tooltip_msg = self._driver.find_element(
            By.XPATH, DashboardPage.tooltip_checkbox_xpath).text
        return tooltip_msg

    def scrolling(self):
        self._driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")