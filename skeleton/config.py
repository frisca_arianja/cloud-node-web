from typing import Tuple, Union
from dotenv import load_dotenv
from os import getenv


def access_env() -> Tuple[bool, Union[Exception, None]]:
    try:
        global TR_USERNAME
        global TR_PASSWORD
        global TR_URL
        global TR_SUCCESS_MSG
        global URL

        load_dotenv(override=True)

        TR_USERNAME = getenv("TR_USERNAME", "")
        TR_PASSWORD = getenv("TR_PASSWORD", "")
        TR_URL = getenv("TR_URL", "https://testrail.io")
        TR_SUCCESS_MSG = getenv(
            "TR_SUCCESS_MSG",
            "Test Executed - Status updated automatically from Selenium test automation."
        )
        URL = getenv("URL", "http://www.google.com")

        return True, None
    except BaseException as exc:
        return False, exc
