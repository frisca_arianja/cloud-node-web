from traceback import TracebackException
from datetime import datetime
from os import makedirs
from os.path import normpath
from typing import Iterable, Union
from logging import error

from .lib.logger.logger import Logger as Logs

# Format: 
# 1. Message is str
#   [timestamp] - Level - message
# 2. Message is Iterable[str]
#   [timestamp] - Level:
#   ....
#   ....
class Logging(Logs):
    pass
