from typing import Dict, Any, List

from .testrail import APIClient, APIError

Data = Dict[str, Any]


class TestrailClient:
    PASSED = 1
    BLOCKED = 2
    RETEST = 4
    FAILED = 5

    def __init__(self, url: str, username: str, password: str) -> APIClient:
        self.__url = url
        self.__username = username
        self.__password = password

        self.__client = APIClient(self.__url)
        self.__client.user = self.__username
        self.__client.password = self.__password

    @property
    def client(self):
        return self.__client

    def get(self, *args, **kwargs):
        return self.client.send_get(*args, **kwargs)

    def post(self, *args, **kwargs):
        return self.client.send_post(*args, **kwargs)

    def get_case(self, case_id: int) -> Data:
        case_data = self.get('get_case/{}'.format(case_id))

        return case_data

    def get_user(self, user_id: int) -> Data:
        user_data = self.get('get_user/{}'.format(user_id))

        return user_data

    def get_section(self, section_id: int) -> Data:
        section_data = self.get(
            'get_section/{}'.format(section_id))

        return section_data

    def get_suite(self, suite_id: int) -> Data:
        suite_data = self.get(
            'get_suite/{}'.format(suite_id))

        return suite_data

    def get_tests(self, run_id: int) -> List[Data]:
        tests_data = self.get('get_tests/{}'.format(run_id))

        return tests_data

    def get_cases_by_section(self, section_id: int) -> List[Data]:
        section_data = self.get_section(section_id)

        suite_id = section_data["suite_id"]
        suite_data = self.get_suite(suite_id)

        project_id = suite_data["suite_id"]

        cases_data = self.get(
            'get_cases/{}&suite_id={}&section_id={}'.format(project_id, suite_id, section_id))

        return cases_data

    def get_cases_by_suite(self, suite_id: int) -> List[Data]:
        suite_data = self.get_suite(suite_id)

        project_id = suite_data["project_id"]

        cases_data = self.get(
            'get_cases/{}&suite_id={}'.format(project_id, suite_id))

        return cases_data

    def get_cases_by_project(self, project_id: int) -> List[Data]:
        cases_data = self.get('get_cases/{}'.format(project_id))

        return cases_data

    def get_cases_by_run(self, run_id: int) -> List[Data]:
        tests_data = self.get_tests(run_id)

        cases_data = []
        for test_data in tests_data:
            case_data = self.get_case(test_data["case_id"])

            cases_data.append(case_data)

        return cases_data

    def send_result_by_case(self, run_id: int, case_id: int, status_id: int, comment: str, elapsed_time: str) -> None:
        data = {
            "status_id": status_id,
            "comment": comment
        }

        if elapsed_time != "0s":
            data["elapsed"] = elapsed_time

        self.post(
            'add_result_for_case/{}/{}'.format(run_id, case_id), data)
