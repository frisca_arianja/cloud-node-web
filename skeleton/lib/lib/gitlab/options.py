from typing import Dict, Any, Callable
from requests import post
from json import dumps, loads

from .auth import Authorization, grant_types


class Options:
    def __init__(self, domain: str = 'http://www.gitlab.com', username: str = None, password: str = None, token: str = None):
        self.__url = domain + '/api/v4'

        if username and password:
            self.__authorization = Authorization(
                grant_types.PASSWORD, {"username": username, "password": password})
        elif token:
            self.__authorization = Authorization(
                grant_types.PRIVATE_ACCESS_TOKEN, {"token": token})

    @property
    def url(self) -> str:
        return self.__url

    @property
    def authorization(self) -> Authorization:
        return self.__authorization
