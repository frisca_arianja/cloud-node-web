from typing import List, Dict, Any, Union, TYPE_CHECKING
from requests import get, post, put
from json import dumps

from .resource import Resource
from .exception import NotFound, Unauthorized, BadRequest, RequestFailed

if TYPE_CHECKING:
    from .options import Options
    from requests import Response


class Issues(Resource):
    def __init__(self, options: 'Options', params: Dict[str, Any] = {}):
        super().__init__(options, 'issues', params)

        self.__id = None
        self.__project_id = None
        self.__group_id = None

    @property
    def issue_id(self) -> Union[int, None]:
        return self.__id

    @property
    def project_id(self) -> Union[int, None]:
        return self.__project_id

    @property
    def group_id(self) -> Union[int, None]:
        return self.__group_id

    @issue_id.setter
    def issue_id(self, issue_id: int):
        self.__id = issue_id

    @project_id.setter
    def project_id(self, project_id: int):
        self.__project_id = project_id

    @group_id.setter
    def group_id(self, group_id: int):
        self.__group_id = group_id

    @project_id.deleter
    def project_id(self):
        self.__project_id = None

    @group_id.deleter
    def group_id(self):
        self.__group_id = None

    def get_issues(self) -> 'Response':
        path = self.path

        if self.project_id:
            path = "/projects/{}{}".format(self.project_id, path)

        if self.group_id:
            path = "/groups/{}{}".format(self.group_id, path)

        resp = get('{}{}{}'.format(self.url, path, self.params),
                   headers=self.authorization.header)

        if resp.status_code == 404:
            raise NotFound("{} not found".format(
                '{}{}{}'.format(self.url, path, self.params)))

        if resp.status_code == 401:
            raise Unauthorized("{} unauthorized".format(
                '{}{}{}'.format(self.url, path, self.params)))

        return resp

    def get_issue(self) -> 'Response':
        issue_id = self.issue_id
        if not issue_id:
            raise Exception("Set issue id first")

        project_id = self.project_id
        if not project_id:
            raise Exception("Set project id first")

        resp = get('{}/projects/{}{}/{}{}'.format(self.url, project_id,
                                                  self.path, issue_id, self.params), headers=self.authorization.header)

        if resp.status_code == 404:
            raise NotFound("{} not found".format(
                '{}/projects/{}{}/{}{}'.format(self.url, project_id, self.path, issue_id, self.params)))

        if resp.status_code == 401:
            raise Unauthorized("{} unauthorized".format(
                '{}/projects/{}{}/{}{}'.format(self.url, project_id, self.path, issue_id, self.params)))

        return resp

    def post_issue(self, issue_data: Dict[str, Any]) -> 'Response':
        project_id = self.project_id
        if not project_id:
            raise Exception("Set project id first")

        try:
            issue_data["title"]
        except KeyError:
            raise BadRequest("title is mandatory")

        headers = {"Content-Type": "application/json",
                   **self.authorization.header}

        resp = post('{}/projects/{}{}'.format(self.url,
                                              project_id, self.path), data=dumps(issue_data), headers=headers)

        if resp.status_code == 401:
            raise Unauthorized("Unauthorized")

        if resp.status_code == 400:
            raise BadRequest("{} bad request".format('{}/projects/{}{}'.format(self.url,
                                                                               project_id, self.path)))

        if resp.status_code == 200:
            raise RequestFailed("Failed to create new issue")

        return resp

    def edit_issue(self, new_issue_data: Dict[str, Any]):
        issue_id = self.issue_id
        if not issue_id:
            raise Exception("Set issue id first")

        project_id = self.project_id
        if not project_id:
            raise Exception("Set project id first")

        headers = {"Content-Type": "application/json",
                   **self.authorization.header}

        resp = put('{}/projects/{}{}/{}'.format(self.url,
                                                project_id, self.path, issue_id), data=dumps(new_issue_data), headers=headers)

        if resp.status_code == 401:
            raise Unauthorized("Unauthorized")

        if resp.status_code == 400:
            raise BadRequest('{}/projects/{}{}/{}'.format(self.url,
                                                          project_id, self.path, issue_id))

        return resp

    def close_issue(self):
        pass

    def delete_issue(self):
        pass
