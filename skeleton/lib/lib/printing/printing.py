from typing import Union

from .colored_print import colored_print


class MessagePrinting:
    DEBUG = "DEBUG"
    SUCCESS = "SUCCESS"
    WARNING = "WARNING"
    ERROR = "ERROR"

    def __init__(self, thread: bool = False):
        self.__messages_collection = []
        self.__thread_printing = thread

    @property
    def thread_printing(self):
        return self.__thread_printing

    @property
    def messages_collection(self):
        return self.__messages_collection

    @property
    def error(self):
        raise RuntimeError('This property has no getter!')

    @property
    def success(self):
        raise RuntimeError('This property has no getter!')

    @property
    def warning(self):
        raise RuntimeError('This property has no getter!')

    @property
    def debug(self):
        raise RuntimeError('This property has no getter!')

    @error.setter
    def error(self, error_message: Union[Exception, str]):
        if self.thread_printing:
            self.messages_collection.append(
                {"status": self.ERROR, "message": error_message})
        else:
            self.__error(error_message)

    @success.setter
    def success(self, success_message: str):
        if self.thread_printing:
            self.messages_collection.append(
                {"status": self.SUCCESS, "message": success_message})
        else:
            self.__success(success_message)

    @warning.setter
    def warning(self, warning_message: Union[Exception, str]):
        if self.thread_printing:
            self.messages_collection.append(
                {"status": self.WARNING, "message": warning_message})
        else:
            self.__warning(warning_message)

    @debug.setter
    def debug(self, debug_message: str):
        if self.thread_printing:
            self.messages_collection.append(
                {"status": self.DEBUG, "message": debug_message})
        else:
            self.__debug(debug_message)

    def __error(self, error_message: Union[Exception, str] = "ERROR", end: str = "\n"):
        if error_message is None:
            error_print = "\u00D7 ERROR" + end
        else:
            error_print = "\u00D7 " + str(error_message) + end
        colored_print(error_print, "red")

    def __success(self, success_message: str = "SUCCESS", end="\n"):
        if success_message is None:
            success_print = "\u2713 SUCCESS" + end
        else:
            success_print = "\u2713 " + str(success_message) + end
        colored_print(success_print, "green")

    def __warning(self, warning_message: Union[Exception, str] = "WARNING", end: str = "\n"):
        if warning_message is None:
            warning_print = "! WARNING" + end
        else:
            warning_print = "! " + str(warning_message) + end
        colored_print(warning_print, "yellow")

    def __debug(self, debug_message: str, end="\n"):
        if debug_message is None:
            debug_message = "NEUTRAL"

        print(debug_message)

    def __str__(self):
        if not self.thread_printing:
            raise Exception("print() is just used for thread printing")

        for mc in self.messages_collection:
            if mc['status'] == self.DEBUG:
                self.__debug(mc['message'])
            if mc['status'] == self.SUCCESS:
                self.__success(mc['message'])
            if mc['status'] == self.WARNING:
                self.__warning(mc['message'])
            if mc['status'] == self.ERROR:
                self.__error(mc['message'])

        return ""
