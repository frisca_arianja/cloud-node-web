from typing import Union, Iterable, Any, Dict, List, TYPE_CHECKING

from .colored_print import colored_print
from .lib.exception.exception import exc_to_generator
from .lib.printing.printing import MessagePrinting

if TYPE_CHECKING:
    from .logging import Logging

M = Union[Exception, str]

class CaseCollectPrint(MessagePrinting):
    def __init__(self, case_id: int, logging: 'Logging' = None):
        self.__case_id = case_id
        self.__log_writing = logging

        self.__logging = False
        if logging:
            self.__logging = True

        super().__init__(True)

    @property
    def case_id(self):
        return self.__case_id

    @property
    def logging(self):
        return self.__logging

    def error_logging(self, error_message: M):
        error_func = exc_to_generator if isinstance(
            error_message, Exception) else str
        self.__log_writing.error = error_func(error_message)

    def warning_logging(self, warning_message: M):
        warn_func = exc_to_generator if isinstance(
            warning_message, Exception) else str
        self.__log_writing.warning = warn_func(warning_message)

    def success_logging(self, success_message: str):
        self.__log_writing.success = success_message

    def __setattr__(self, attr, value):
        if attr in ["success", "warning", "error"]:
            message_value = log_value = value
            if len(value) > 1 and type(value) is tuple:
                message_value, log_value = value

            super(CaseCollectPrint, self).__setattr__(attr, message_value)

            if self.logging:
                getattr(self, "{}_logging".format(attr))(log_value)
        else:
            super(CaseCollectPrint, self).__setattr__(attr, value)

    def __str__(self):
        if self.messages_collection:
            print("------------------ C{} ------------------".format(self.case_id))

        return super().__str__()


def warning_print(warn_message: str = "", end: str = "\n") -> None:
    if warn_message == "" or warn_message is None:
        warn_message = "! WARNING" + end
    else:
        warn_message = "! " + warn_message + end
    colored_print(warn_message, "yellow")


def success_print(success_message: str = "", end: str = "\n") -> None:
    if success_message == "" or success_message is None:
        success_message = "\u2713 SUCCESS" + end
    else:
        success_message = "\u2713 " + success_message + end
    colored_print(success_message, "green")


def error_print(error_message: str = "", end: str = "\n") -> None:
    if error_message == "" or error_message is None:
        error_message = "\u00D7 ERROR" + end
    else:
        error_message = "\u00D7 " + error_message + end
    colored_print(error_message, "red")
