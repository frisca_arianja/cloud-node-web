webdriver-manager>=1.8.2
selenium>=3.141.0
Faker>=2.0.1
environs>=6.1.0
python-dotenv>=0.10.3
requests>=2.22.0
